<?php

/**
 * @file
 * Archibald Curricula Mapping module API documentation.
 */

/**
 * @defgroup archibald_curr_mapping_api Archibald Curricula Mapping
 * @{
 * Archibald can use multiple curricula, like PER or LP21. However, working on
 * multiple curricula can be tedious. The dsb API provides a mechanism to
 * suggest corresponding elements in a curriculum based on a user's selection in
 * another curriculum. The Archibald Curricula Mapping module provides the logic
 * and user interface to do so.
 *
 * @see archibald_api
 * @see https://dsb-api.educa.ch/latest/doc
 * @}
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Implements hook_archibald_curr_mapping().
 *
 * A module providing a curriculum integration in Archibald can act as a source
 * for mapping suggestions. By implementing this hook, these modules can be
 * registered with Archibald Curricula Mapping. The way it works is as follows:
 *
 * 1. A user makes a selection in curriculum A, provided by module A.
 * 2. The module B also provides a curriculum, curriculum B. Module B implements
 *    this hook, and registers itself as being able to provide suggestions for
 *    curriculum A.
 * 3. Archibald Curricula Mapping calls the suggestion_callback registered by
 *    module B, and retrieves suggestions for curriculum B, based on the
 *    selection in curriculum A. These are then presented to the user.
 *
 * @see hook_archibald_curr_mapping_alter()
 * @see archibald_curr_mapping_provide_suggestions()
 *
 * @return array
 *     An array, keyed by curriculum machine name. Each item in the array can
 *     contain the following keys:
 *     - label (required): The human readable name of the curriculum.
 *     - provides_for (required): A list of curricula, in human readable format,
 *       for which the module can provide suggestions. Each item is keyed by
 *       the curriculum machine name. It is important to understand that the
 *       suggestions are provided for the curriculum itself, not for the
 *       curricula listed in this key. The curricula listed here describe for
 *       which curricula the module is able to coherently map its own items,
 *       thus providing suggestions.
 *     - suggestion_callback: A function to call when requesting suggestions.
 *       This is only used if the provides_for option is set. The callback
 *       will get passed the target curriculum name, as well as the item ID
 *       that was selected. See archibald_curr_mapping_provide_suggestions()
 *       for an example implementation.
 *
 * @ingroup archibald_curr_mapping_api
 */
function hook_archibald_curr_mapping() {
  $provides_for = array();
  if (module_exists('archibald_lp21')) {
    $provides_for['lp21'] = t("Lehrplan 21");
  }
  if (module_exists('archibald_per')) {
    $provides_for['per'] = t("PER");
  }

  return array(
    'classification_system' => array(
      'label' => t("Classification systems"),
      'provides_for' => $provides_for,
      'suggestion_callback' => 'archibald_curr_mapping_provide_suggestions',
    ),
  );
}

/**
 * Implements hook_archibald_curr_mapping_alter().
 *
 * @see hook_archibald_curr_mapping()
 *
 * @param array &$options
 *     See hook_archibald_curr_mapping() for information on the format of this
 *     array.
 *
 * @ingroup archibald_curr_mapping_api
 */
function hook_archibald_curr_mapping_alter(&$options) {
  $options['per']['provides_for']['classification_system'] = t("Classification systems");
}

/**
 * Provide suggestions based on selection.
 *
 * This is an example implementation of a callback for providing suggestions
 * for other curricula based on a selection. The function gets passed the
 * "source" curriculum, the "target" curriculum, as well as the item that was
 * selected. Based on this information, the module can determine which one(s) of
 * its own items are "equivalent", and can be suggested for automated selection.
 *
 * @see hook_archibald_curr_mapping()
 *
 * @param string $source
 *    The source curriculum.
 * @param string $target
 *    The curriculum for which to provide suggestions.
 * @param string $item_id
 *    The item in the source curriculum that was just selected.
 *
 * @return array
 *    A list of suggestions. Each suggestion is an array with the following
 *    keys:
 *    - id: The item identifier in the target curriculum.
 *    - label: A human-readable string, which will be presented to the user as
 *      an explanation for the suggestion. Usually, a "path" is sufficient.
 *      E.g.: "Cycle 1 > Arts > Visual arts"
 */
function archibald_curr_mapping_provide_suggestions($source, $target, $item_id) {

}

/**
 * @} End of "addtogroup hooks".
 */
