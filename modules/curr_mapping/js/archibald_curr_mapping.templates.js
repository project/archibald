
(function(Archibald) {

Archibald.templates = Archibald.templates || (Archibald.templates = new Object());

// Add our own templates.
Archibald.templates.currMappingSuggestionsWrapper = '\
  <div class="archibald-curricula-field__editor__suggestions messages warning">\
    <div class="archibald-curricula-field__editor__suggestions__content"></div>\
  </div>\
';

Archibald.templates.currMappingSuggestions = '\
  <% for (var curriculum in items) { %>\
    <% if (items[curriculum].suggestions.length) {%>\
      <div class="archibald-curricula-field__editor__suggestions__wrapper">\
        <strong><%= Drupal.formatPlural(\
          items[curriculum].suggestions.length,\
          "This item was automatically added to %curriculum",\
          "The following @count items were automatically added to %curriculum", {\
          "%curriculum": items[curriculum].label\
        }, { context: "archibald_curr_mapping:view" }) %></strong>\
        <ul>\
          <% for (var i = 0, len = items[curriculum].suggestions.length; i < len; i++) { %>\
          <li>\
            <label>\
              <input\
                type="checkbox"\
                data-curriculum-id="<%= Drupal.checkPlain(curriculum) %>"\
                data-curriculum-label="<%= Drupal.checkPlain(items[curriculum].label) %>"\
                data-item-id="<%= Drupal.checkPlain(items[curriculum].suggestions[i].id) %>"\
                checked\
                class="archibald-curricula-field__editor__suggestions__content__checkbox"\
                title="<%= Drupal.t("Add / remove this suggestion", {}, { context: "archibald_curr_mapping:view" }) %>" />\
              <%= Drupal.checkPlain(items[curriculum].suggestions[i].label) %>\
              <span class="archibald-curricula-field__editor__suggestions__content__confirm-message"></span>\
            </label>\
          </li>\
          <% } %>\
        </ul>\
      </div>\
    <% } %>\
  <% } %>\
';

})(window.ArchibaldCurriculum || (window.ArchibaldCurriculum = new Object()));
