<?php

/**
 * @file
 * Settings and administration callbacks for Archibald Curricula Mapping.
 */

/**
 * Module settings form callback.
 */
function archibald_curr_mapping_admin_settings_form($form, $form_state) {
  $available_mappings = archibald_curr_mapping_get_mappings();
  foreach ($available_mappings as $curriculum => $info) {
    if (!empty($info['provides_for'])) {
      $form[$curriculum] = array(
        '#type' => 'fieldset',
        '#title' => $info['label'],
      );

      foreach ($info['provides_for'] as $key => $label) {
        $form[$curriculum]["archibald_curr_mapping__from_{$key}_to_{$curriculum}"] = array(
          '#type' => 'checkbox',
          '#title' => t("Provide suggestions based on selections in %from", array(
            '%from' => $label,
          )),
          '#default_value' => variable_get("archibald_curr_mapping__from_{$key}_to_{$curriculum}", 0),
        );
      }
    }
  }

  return system_settings_form($form);
}
