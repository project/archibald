
(function(Archibald) {

Archibald.templates = Archibald.templates || (Archibald.templates = new Object());

// We want to alter the display of certain items.
Archibald.templates.summaryList = '\
  <li\
    data-model-id="<%= id %>"\
    class="\
    archibald-curriculum-ui-summary__list__item\
    archibald-curriculum-ui-summary__list__item--<%= type %>\
    "\
  >\
    <span>\
      <% for (var i in name) { %>\
        <%= name[i] %>\
        <% if (i < name.length - 1) {%><hr /><% } %>\
      <% } %>\
    </span>\
    <%= children %>\
  </li>\
';

// Add our own templates.
Archibald.templates.lp21CyclesForm = '\
  <div class="archibald-curriculum-ui__row archibald-lp21-cycles">\
    <span class="archibald-lp21-cycles__label"><%= Drupal.t("Select cycle(s)") %>:</span>\
    <label class="archibald-lp21-cycles__cycle archibald-lp21-cycles__cycle--cycle-1">\
      <input type="checkbox" value="1" /><%= Drupal.t("Cycle 1") %>\
    </label>\
    <label class="archibald-lp21-cycles__cycle archibald-lp21-cycles__cycle--cycle-2">\
      <input type="checkbox" value="2" /><%= Drupal.t("Cycle 2") %>\
    </label>\
    <label class="archibald-lp21-cycles__cycle archibald-lp21-cycles__cycle--cycle-3">\
      <input type="checkbox" value="3" /><%= Drupal.t("Cycle 3") %>\
    </label>\
  </div>\
';

Archibald.templates.lp21CyclesSummary = '\
  <div class="archibald-lp21-summary-cycles">\
    <span class="archibald-lp21-summary-cycles__cycle archibald-lp21-summary-cycles__cycle--cycle-1" title="<%= Drupal.t("Has cycle 1") %>" />\
    <span class="archibald-lp21-summary-cycles__cycle archibald-lp21-summary-cycles__cycle--cycle-2" title="<%= Drupal.t("Has cycle 2") %>" />\
    <span class="archibald-lp21-summary-cycles__cycle archibald-lp21-summary-cycles__cycle--cycle-3" title="<%= Drupal.t("Has cycle 3") %>" />\
  </div>\
';

})(window.ArchibaldCurriculum || (window.ArchibaldCurriculum = new Object()));
