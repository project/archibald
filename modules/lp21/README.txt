========================================
Archibald LP21 - Lehrplan 21 integration
========================================

In order to use the LP21 integration, you must refresh the curriculum data cache. This is done by visiting the following path: admin/archibald-lp21/load-lp21-data

You can always check the cache status under admin/reports/status.
