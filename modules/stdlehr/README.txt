=====================================================================
Archibald Classification Systems - Classification Systems integration
=====================================================================

In order to use the Classification Systems integration, you must refresh the curriculum data cache. This is done by visiting the following path: admin/archibald-stdlehr/load-stdlehr-data

You can always check the cache status under admin/reports/status.

About the name
==============

This module originally implemented the Standardlehrplan. But since its deprecation and replacement with the Classification Systems, this module was re-purposed. Its machine name, however, remained for BC reasons.
