================================================
Archibald PER - Plan d'études Romand integration
================================================

In order to use the PER integration, you must refresh the curriculum data cache. This is done by visiting the following path: admin/archibald-per/load-per-data

You can always check the cache status under admin/reports/status.
