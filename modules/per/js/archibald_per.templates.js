
(function(Archibald) {

Archibald.templates = Archibald.templates || (Archibald.templates = new Object());

// We want to alter the display of certain items.
Archibald.templates.item = '\
  <% if (editable && (typeof data === "undefined" || typeof data.isSelectable === "undefined" || data.isSelectable)) { %>\
    <input type="checkbox" value="model-<%= id %>"<% if (active) { %> checked<% } %>/>\
  <% } %>\
  <% for (var i in name) { %>\
    <%= name[i] %>\
    <% if (i < name.length - 1) {%><hr /><% } %>\
  <% } %>\
  <% if (hasChildren) { %>\
    <i class="icon-chevron-right" />\
  <% } %>\
';

Archibald.templates.summaryList = '\
  <li\
    data-model-id="<%= id %>"\
    class="\
    archibald-curriculum-ui-summary__list__item\
    archibald-curriculum-ui-summary__list__item--<%= type %>\
    <% if (typeof data !== "undefined" && typeof data.isGroup !== "undefined" && data.isGroup) { %>archibald-curriculum-ui-summary__list__item--isGroup<%  } %>\
    "\
  >\
    <span>\
      <% for (var i in name) { %>\
        <%= name[i] %>\
        <% if (i < name.length - 1) {%><hr /><% } %>\
      <% } %>\
    </span>\
    <%= children %>\
  </li>\
';

})(window.ArchibaldCurriculum || (window.ArchibaldCurriculum = new Object()));
