<?php

/**
 * @file
 * Default qTip definitions.
 */

/**
 * Implements hook_qtip_default_qtips().
 */
function archibald_help_qtip_default_qtips() {
  $qtips = array();

  $qtip = new stdClass();
  $qtip->disabled = FALSE;
  $qtip->api_version = 1.0;
  $qtip->machine_name = 'archibald_help';
  $qtip->name = 'Archibald Help';
  $qtip->settings = array(
    'content' => array(
      'button' => 0,
    ),
    'style' => array(
      'tip' => array(
        'width' => '6',
        'height' => '6',
        'border' => '',
        'corner_position' => '',
        'mimic' => '',
        'offset' => '',
        'corner' => 0,
      ),
      'classes' => 'qtip-dark',
      'classes_custom' => '',
      'shadow' => 'qtip-shadow',
      'rounded_corners' => 'qtip-rounded',
    ),
    'position' => array(
      'at' => 'bottom right',
      'my' => '',
      'viewport' => 0,
      'target' => 0,
      'adjust' => array(
        'method' => '',
      ),
    ),
    'show' => array(
      'event' => array(
        'click' => 'click',
        'mouseenter' => 0,
        'focus' => 0,
      ),
      'solo' => 1,
      'ready' => 0,
    ),
    'hide' => array(
      'event' => array(
        'mouseleave' => 'mouseleave',
        'click' => 'click',
        'unfocus' => 0,
        'blur' => 0,
      ),
      'fixed' => 1,
      'delay' => '200',
      'inactive' => '',
    ),
    'miscellaneous' => array(
      'button_title_text' => '',
    ),
  );

  $qtips['archibald_help'] = $qtip;

  return $qtips;
}
