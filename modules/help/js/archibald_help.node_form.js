
(function($, Drupal) {

  Drupal.behaviors.archibaldHelp = {

    attach: function(context, settings) {
      var $qtips = $('.archibald-help-tooltip.qtip-link', context);
      if ($qtips.length) {
        $qtips.click(function() {
          return false;
        });
      }
    }

  };

})(jQuery, Drupal);
