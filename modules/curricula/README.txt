=======================================================================
Archibald Curricula - custom form element for curriculum data selection
=======================================================================

This module allows a site to use a custom form element, whose type is "archibald_curricula". This form element exposes the Curricula UI component, which is a small, self-contained Backbone application to facilitate curriculum data selection.

More information can be found here: https://github.com/educach/curricula-ui

Dependency installation
=======================

There are 2 ways to install the module's dependencies.

* Using Drush only
* Downloading third-party libraries "by hand" and uploading them to your server via FTP

You can choose either method, but you must choose at least one.

Using Drush only
----------------

First, install Drush on your host (http://docs.drush.org/en/master/install/).

Change directory inside your Drupal site and call:

drush archibald-curricula-libraries-setup

This will put all libraries inside sites/all/libraries. If you wish to use another libraries folder, simply pass it to the command:

drush archibald-curricula-libraries-setup sites/example.com/libraries

Go to your site status report (admin/reports/status) and make sure Archibald has found all required libraries.


Downloading third-party libraries and uploading them by FTP
-----------------------------------------------------------

This module requires the Curricula UI Javascript component. But Curricula UI itself has other dependencies. Basically, all libraries should be uploaded to your current site's libraries folder. This is located in:

sites/*/libraries

For example:

- sites/all/libraries
- sites/example.com/libraries
- etc

If you don't see a "libraries" folder, simply create one.

After that, all downloaded libraries must be put in their own folder. You will have something like:

sites/*/libraries/{library_name}/

Curricula UI:
1. Download it from https://github.com/educach/curricula-ui/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "curricula-ui" (so you will have sites/*/libraries/curricula-ui/)

Backbone:
1. Download it from https://github.com/jashkenas/backbone/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "backbone" (so you will have sites/*/libraries/backbone/)

Underscore:
1. Download it from https://github.com/jashkenas/underscore/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "underscore" (so you will have sites/*/libraries/underscore/)

nanoScrollerJS:
1. Download it from https://github.com/jamesflorentino/nanoScrollerJS/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "nanoScrollerJS" (so you will have sites/*/libraries/nanoScrollerJS/)

Go to your site status report (admin/reports/status) and make sure Archibald Curricula has found all required libraries.
