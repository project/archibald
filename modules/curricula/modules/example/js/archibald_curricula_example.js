(function($) {

Drupal.behaviors.archibaldCurriculaExample = {

  attach: function(context, settings) {
    var wrappers = $('.archibald-curricula-example-field-wrapper:not(.js-processed)', context);
    if (wrappers.length) {
      wrappers.each(function() {
        var app = new ArchibaldCurriculum.Core(settings.archibaldCurriculaExample.data),
            $this = $(this);

        app.setSettings(settings.archibaldCurriculaExample.settings);
        app.setWrapper($this);
        app.createRootColumn(true);
        app.activateResponsiveLogic();

        $this.addClass('js-processed');
      });
    }
  },

};

})(jQuery);
