<?php

/**
 * @file
 * Drush commands for Archibald Curricula.
 */

/**
 * Implements hook_drush_command().
 */
function archibald_curricula_drush_command() {
  return array(
    'archibald-curricula-libraries-setup' => array(
      'description' => dt("Helper function to download and setup the different required libraries."),
      'aliases' => array('acls'),
      'arguments' => array(
        'location' => dt("The location to which the libraries should be downloaded. Defaults to sites/all/libraries."),
      ),
      'options' => array(
        'force' => dt("Whether to force the download of the libraries. This removes existing libraries and replace them with fresh downloads."),
      ),
      'callback' => 'archibald_curricula_drush_libraries_setup',
    ),
  );
}

/**
 * Download required libraries.
 *
 * @see _archibald_drush_libraries_setup()
 *
 * @param string $location
 *    (optional) Where to download the libraries. Defaults to
 *    'sites/all/libraries'.
 */
function archibald_curricula_drush_libraries_setup($location = 'sites/all/libraries') {
  // We don't depend on the Archibald base module. But we do want to use its
  // Drush functionality. Because we ship as a sub module anyway, load the code
  // like this.
  require_once dirname(__FILE__) . '/../../../drush/archibald.drush.inc';
  $force = !!drush_get_option('force', FALSE);
  return _archibald_drush_libraries_setup(archibald_curricula_libraries_info(), $location, $force);
}
