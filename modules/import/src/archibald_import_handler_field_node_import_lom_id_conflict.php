<?php

/**
 * @file
 * Contains archibald_import_handler_field_node_import_lom_id_conflict.
 */

class archibald_import_handler_field_node_import_lom_id_conflict extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  function render($values) {
    if (!empty($values->{$this->field_alias})) {
      $node = archibald_lomch_description_load_by_lom_id($values->{$this->field_alias});

      $output = '<div class="messages error">';
      $output .= t("LOM ID %id already belongs to a description: !title", array(
        '%id' => $values->{$this->field_alias},
        '!title' => l("{$node->title} (nid {$node->nid})", "node/{$node->nid}"),
      ));

      if (!empty($values->nid) && !empty($values->node_title)) {
        $output .= '<br />' . t("You can replace the data from the current description above (%old_title, nid @old_nid) with the data from this newly imported one (%new_title, nid @new_nid). To do this, click <a href='!link'>here</a>.", array(
          '%old_title' => $node->title,
          '@old_nid' => $node->nid,
          '%new_title' => $values->node_title,
          '@new_nid' => $values->nid,
          '!link' => url("archibald-import/replace/{$node->nid}/{$values->nid}", array(
            'query' => array('destination' => current_path()),
          )),
        ));
      }

      $output .= '</div>';
      return $output;
    }
    else {
      return '-';
    }
  }
}
