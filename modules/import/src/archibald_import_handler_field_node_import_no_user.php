<?php

/**
 * @file
 * Contains archibald_import_handler_field_node_import_no_user.
 */

class archibald_import_handler_field_node_import_no_user extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  function render($values) {
    if (!empty($values->{$this->field_alias})) {
      if (!$account = user_load_by_mail($values->{$this->field_alias})) {
        $account = user_load_by_name($values->{$this->field_alias});
      }
      $output = '<div class="messages ' . ($account ? 'status' : 'warning') . '">';
      if ($account) {
        $output .= t("Possible match found for %orig: %user.", array(
          '%user' => $account->name,
          '%orig' => $values->{$this->field_alias},
        )) . '<br />' . t("Use <em>Assign to author</em> to solve issue.");
      }
      else {
        $output .= t("User %orig doesn't exist.", array(
          '%orig' => $values->{$this->field_alias},
        ));
      }
      $output .= '</div>';
      return $output;
    }
    else {
      return '-';
    }
  }
}
