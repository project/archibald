<?php

/**
 * @file
 * Contains archibald_import_handler_field_node_import_no_partner.
 */

class archibald_import_handler_field_node_import_no_partner extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  function render($values) {
    if (!empty($values->{$this->field_alias})) {
      $partner = archibald_partner_load_by_name($values->{$this->field_alias});
      $output = '<div class="messages ' . ($partner ? 'status' : 'warning') . '">';
      if ($partner) {
        $output .= t("Found partner %partner.", array(
          '%partner' => $values->{$this->field_alias},
        )) . '<br />' . t("Use <em>Assign to partner</em> to solve issue.");
      }
      else {
        $output .= t("Partner %partner is not available.", array(
          '%partner' => $values->{$this->field_alias},
        ));
      }
      $output .= '</div>';
      return $output;
    }
    else {
      return '-';
    }
  }
}
