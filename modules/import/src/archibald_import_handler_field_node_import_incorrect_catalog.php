<?php

/**
 * @file
 * Contains archibald_import_handler_field_node_import_incorrect_catalog.
 */

class archibald_import_handler_field_node_import_incorrect_catalog extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  function render($values) {
    if (!empty($values->{$this->field_alias})) {
      $found = FALSE;
      $field = field_info_field('archibald_publication_catalogs');
      if (in_array($values->{$this->field_alias}, array_keys($field['settings']['allowed_values']))) {
        $found = TRUE;
      }
      $output = '<div class="messages ' . ($found ? 'status' : 'warning') . '">';
      if ($found) {
        $output .= t("Found catalog %catalog.", array(
          '%catalog' => $values->{$this->field_alias},
        )) . '<br />' . t("Use <em>Assign to catalog(s)</em> to solve issue.");
      }
      else {
        $output .= t("Catalog %catalog is not available.", array(
          '%catalog' => $values->{$this->field_alias},
        ));
      }
      $output .= '</div>';
      return $output;
    }
    else {
      return '-';
    }
  }
}
