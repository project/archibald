<?php

/**
 * @file
 * Default views for the Archibald Import module.
 */

/**
 * Implements hook_views_default_views().
 */
function archibald_import_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'archibald_unfinished_imports';
  $view->description = 'List of unfinished imports.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Archibald unfinished imports';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Descriptions';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'import archibald descriptions';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Bulk operations: Content */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'node';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['label'] = '';
  $handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '1';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::archibald_assign_to_catalog_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::archibald_assign_to_partner_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_assign_owner_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 1,
      'label' => 'Assign to author',
    ),
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 1,
      'label' => 'Delete',
    ),
  );
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '[title] (nid [nid])';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Conflicting LOM ID */
  $handler->display->display_options['fields']['lom_id_conflict']['id'] = 'lom_id_conflict';
  $handler->display->display_options['fields']['lom_id_conflict']['table'] = 'archibald_import_errors';
  $handler->display->display_options['fields']['lom_id_conflict']['field'] = 'lom_id_conflict';
  $handler->display->display_options['fields']['lom_id_conflict']['label'] = 'LOM ID';
  /* Field: Content: Incorrect catalog */
  $handler->display->display_options['fields']['incorrect_catalog']['id'] = 'incorrect_catalog';
  $handler->display->display_options['fields']['incorrect_catalog']['table'] = 'archibald_import_errors';
  $handler->display->display_options['fields']['incorrect_catalog']['field'] = 'incorrect_catalog';
  $handler->display->display_options['fields']['incorrect_catalog']['label'] = 'Catalog(s)';
  /* Field: Content: Inexisting partner */
  $handler->display->display_options['fields']['no_partner']['id'] = 'no_partner';
  $handler->display->display_options['fields']['no_partner']['table'] = 'archibald_import_errors';
  $handler->display->display_options['fields']['no_partner']['field'] = 'no_partner';
  $handler->display->display_options['fields']['no_partner']['label'] = 'Partner';
  /* Field: Content: Inexisting user */
  $handler->display->display_options['fields']['no_user']['id'] = 'no_user';
  $handler->display->display_options['fields']['no_user']['table'] = 'archibald_import_errors';
  $handler->display->display_options['fields']['no_user']['field'] = 'no_user';
  $handler->display->display_options['fields']['no_user']['label'] = 'Author';
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = '';
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
  /* Field: Content: Delete link */
  $handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
  $handler->display->display_options['fields']['delete_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
  $handler->display->display_options['fields']['delete_node']['label'] = '';
  $handler->display->display_options['fields']['delete_node']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'archibald_lomch_description' => 'archibald_lomch_description',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Inexisting user */
  $handler->display->display_options['filters']['no_user']['id'] = 'no_user';
  $handler->display->display_options['filters']['no_user']['table'] = 'archibald_import_errors';
  $handler->display->display_options['filters']['no_user']['field'] = 'no_user';
  $handler->display->display_options['filters']['no_user']['operator'] = '!=';
  $handler->display->display_options['filters']['no_user']['group'] = 2;
  /* Filter criterion: Content: Conflicting LOM ID */
  $handler->display->display_options['filters']['lom_id_conflict']['id'] = 'lom_id_conflict';
  $handler->display->display_options['filters']['lom_id_conflict']['table'] = 'archibald_import_errors';
  $handler->display->display_options['filters']['lom_id_conflict']['field'] = 'lom_id_conflict';
  $handler->display->display_options['filters']['lom_id_conflict']['operator'] = '!=';
  $handler->display->display_options['filters']['lom_id_conflict']['group'] = 2;
  /* Filter criterion: Content: Inexisting partner */
  $handler->display->display_options['filters']['no_partner']['id'] = 'no_partner';
  $handler->display->display_options['filters']['no_partner']['table'] = 'archibald_import_errors';
  $handler->display->display_options['filters']['no_partner']['field'] = 'no_partner';
  $handler->display->display_options['filters']['no_partner']['operator'] = '!=';
  $handler->display->display_options['filters']['no_partner']['group'] = 2;
  /* Filter criterion: Content: Incorrect catalog */
  $handler->display->display_options['filters']['incorrect_catalog']['id'] = 'incorrect_catalog';
  $handler->display->display_options['filters']['incorrect_catalog']['table'] = 'archibald_import_errors';
  $handler->display->display_options['filters']['incorrect_catalog']['field'] = 'incorrect_catalog';
  $handler->display->display_options['filters']['incorrect_catalog']['operator'] = '!=';
  $handler->display->display_options['filters']['incorrect_catalog']['group'] = 2;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/archibald/description/unfinished-imports';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Unfinished imports';
  $handler->display->display_options['menu']['weight'] = '5';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['archibald_unfinished_imports'] = array(
    t('Master'),
    t('Unfinished imports', array(), array('context' => 'archibald:views')),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('- Choose an operation -'),
    t('Assign to author'),
    t('Delete'),
    t('Title'),
    t('LOM ID'),
    t('Catalog(s)'),
    t('Partner'),
    t('Author'),
    t('Page'),
  );

  // DO NOT FORGET! Some translatables require a context! When copy-pasting a
  // new version of the view, don't forget the following modifications:
  // - t('Unfinished imports', array(), array('context' => 'archibald:views'))
  $export['archibald_unfinished_imports'] = $view;

  return $export;
}
