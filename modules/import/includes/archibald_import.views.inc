<?php

/**
 * @file
 * Views hook definitions for the Archibald Import module.
 */

/**
 * Implements hook_views_data().
 */
function archibald_import_views_data() {
  $data = array();

  $data['archibald_import_errors'] = array('table' => array());
  $data['archibald_import_errors']['table']['group'] = t("Content");
  $data['archibald_import_errors']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'node_revision' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['archibald_import_errors']['no_user'] = array(
    'title' => t("Inexisting user"),
    'help' => t("The imported data could not be assigned to the desired account."),
    'field' => array(
      'handler' => 'archibald_import_handler_field_node_import_no_user',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['archibald_import_errors']['no_partner'] = array(
    'title' => t("Inexisting partner"),
    'help' => t("The imported data could not be linked to the desired partner."),
    'field' => array(
      'handler' => 'archibald_import_handler_field_node_import_no_partner',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['archibald_import_errors']['incorrect_catalog'] = array(
    'title' => t("Incorrect catalog"),
    'help' => t("The imported data could not assigned to the desired catalog(s)."),
    'field' => array(
      'handler' => 'archibald_import_handler_field_node_import_incorrect_catalog',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['archibald_import_errors']['lom_id_conflict'] = array(
    'title' => t("Conflicting LOM ID"),
    'help' => t("The imported data has the same LOM ID as another description."),
    'field' => array(
      'handler' => 'archibald_import_handler_field_node_import_lom_id_conflict',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}