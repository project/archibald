<?php

/**
 * @file
 * Archibald Import module API documentation.
 */

/**
 * @defgroup archibald_import_api Archibald Import
 * @{
 * Archibald can import descriptions in JSON format, and map all data to the
 * "archibald_lomch_description" content type. Other modules can alter the
 * data to be imported to add or change data before it's saved.
 * @}
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Implements hook_archibald_json_import_alter().
 *
 * Alter data to be imported.
 *
 * @see archibald_import_from_json()
 *
 * @param object $node
 *    The node that will be, eventually, imported.
 * @param array $context
 *    A context array, which contains the following keys:
 *    - json: The raw JSON that is imported.
 *    - meta_json: If any meta-data is imported as well, this contains the raw
 *      meta-data JSON file.
 *    - revision_json: If any revisions are imported as well, this contains the
 *      raw revisions JSON file.
 *    - files: A list of files. If a file was found in the JSON (like for the
 *      preview image), it will be pre-fetched and stored in Drupal's files
 *      table. The array is keyed by filepath or URL, as preset in the JSON,
 *      with the value being the file ID in the database. For example:
 *      @code
 *      array(
 *        'zip://file_1.png' => 12,
 *        'http://example.com/logo.jpg' => 13,
 *      )
 *      @endcode
 *    A module can decide to alter the $context array and add a 'abort' key. If
 *    this value is set to true, the import will be aborted. This does not abort
 *    values that are already saved, like taxonomy terms or files. If an import
 *    is aborted, it is advised that the module puts up some kind of error
 *    message to explain why it did so.
 *
 * @ingroup archibald_import_api
 */
function hook_archibald_json_import_alter($node, $context) {
  $context['abort'] = TRUE;
}

/**
 * Implements hook_archibald_json_import_revision_alter().
 *
 * Similar to hook_archibald_json_import_alter(), but is called for each
 * revision that is imported. This hook is only called when revision data is
 * actually present.
 *
 * @ingroup archibald_import_api
 */
function hook_archibald_json_import_revision_alter($node, $context) {
  $context['abort'] = TRUE;
}

/**
 * @} End of "addtogroup hooks".
 */
