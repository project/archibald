<?php

/**
 * @file
 * Default rules.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function archibald_proposals_default_rules_configuration() {
  $rules = array();

  $rules['rules_archibald_proposals_created_conditions'] = rules_import(
'{ "rules_archibald_proposals_created_conditions" : {
    "LABEL" : "Archibald proposal created (conditions)",
    "PLUGIN" : "and",
    "OWNER" : "rules",
    "TAGS" : [ "archibald", "lomch", "lomch_description" ],
    "REQUIRES" : [ "rules" ],
    "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
    "AND" : [
      { "entity_is_of_bundle" : {
          "entity" : [ "node" ],
          "type" : "node",
          "bundle" : { "value" : { "archibald_lomch_description" : "archibald_lomch_description" } }
        }
      },
      { "user_has_role" : { "account" : [ "node:author" ], "roles" : { "value" : { "1" : "1" } } } }
    ]
  }
}');

    $rules['rules_archibald_proposals_created_actions'] = rules_import(
'{ "rules_archibald_proposals_created_actions" : {
    "LABEL" : "Archibald proposal created (actions)",
    "PLUGIN" : "action set",
    "OWNER" : "rules",
    "TAGS" : [ "archibald", "lomch", "lomch_description" ],
    "REQUIRES" : [ "archibald_proposals", "rules" ],
    "USES VARIABLES" : {
      "node" : { "label" : "Node", "type" : "node" },
      "date" : { "label" : "Date", "type" : "date" }
    },
    "ACTION SET" : [
      { "archibald_proposals_rules_load_users_with_perm" : {
          "USING" : { "permission" : "get notified on proposal creation" },
          "PROVIDE" : { "users" : { "users" : "Users" } }
        }
      },
      { "LOOP" : {
          "USING" : { "list" : [ "users" ] },
          "ITEM" : { "user" : "User" },
          "DO" : [
            { "mail" : {
                "to" : [ "user:mail" ],
                "subject" : "Proposal created",
                "message" : "Hi [user:name],\r\n\r\nA new proposal was created on [date:value], called \u0022[node:title]\u0022 ([node:url]).\r\n\r\nIt was suggested by [node:proposal-name] ([node:proposal-email]), who left the following message:\r\n[node:proposal-message]\r\n\r\nYou can manage proposals here: [site:url]?q=user/login&destination=admin/archibald/description/proposals\r\n\r\nBest regards,\r\n\r\nArchibald",
                "language" : [ "" ]
              }
            }
          ]
        }
      },
      { "drupal_message" : {
          "message" : "Thank you [node:proposal-name] for your submission. We will review it and get in touch with you as soon as possible.",
          "repeat" : "0"
        }
      },
      { "mail" : {
          "to" : "[node:proposal-email]",
          "subject" : "We received your submissions \u0022[node:title]\u0022",
          "message" : "Dear [node:proposal-name],\r\n\r\nWe received your submission for \u0022[node:title]\u0022. We will review your submission and contact you as soon as possible.\r\n\r\nSincerely,\r\n\r\nThe [site:name] team",
          "from" : "[site:mail]",
          "language" : [ "" ]
        }
      }
    ]
  }
}');

  $rules['rules_archibald_proposals_created'] = rules_import(
'{ "rules_archibald_proposals_created" : {
    "LABEL" : "Archibald proposal created",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "archibald", "lomch", "lomch_description" ],
    "REQUIRES" : [ "rules" ],
    "ON" : { "node_insert" : [] },
    "IF" : [
      { "component_rules_archibald_proposals_created_conditions" : { "node" : [ "node" ] } }
    ],
    "DO" : [
      { "component_rules_archibald_proposals_created_actions" : { "node" : [ "node" ], "date" : "now" } }
    ]
  }
}');

  return $rules;
}
