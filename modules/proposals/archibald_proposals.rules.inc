<?php

/**
 * @file
 * Rules API integration.
 */

/**
 * Action callback for archibald_proposals_rules_load_users_with_perm.
 */
function archibald_proposals_rules_load_users_with_perm($permission) {
  $users = array();

  $result = db_query("
  SELECT DISTINCT  u.uid
             FROM  {users} u
        LEFT JOIN  {users_roles} r
               ON  u.uid = r.uid
        LEFT JOIN  {role_permission} p
               ON  r.rid = p.rid
            WHERE  u.uid != 0
              AND  p.permission IS NOT NULL
              AND  p.permission = :permission
  ", array(
    ':permission' => $permission,
  ));

  while ($uid = $result->fetchField()) {
    $users[$uid] = user_load($uid);
  }

  return array(
    'users' => $users,
  );
}
