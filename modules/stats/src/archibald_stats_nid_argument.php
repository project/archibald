<?php

/**
 * @file
 * Contains archibald_stats_nid_argument.
 */

class archibald_stats_nid_argument extends views_handler_argument {

  /**
   * @{inheritdoc}
   */
  function query($group_by = FALSE) {
    // We need to load the node, and check if it has a lom_id property. If it
    // does, than we simply treat it just as a archibald_stats_lomid_filter on
    // the lomId field.
    $node = node_load($this->argument);
    if (!empty($node->lom_id)) {
      $filter = new archibald_stats_lomid_filter();
      $options = $this->options + array(
        'operator' => '=',
        'value' => $node->lom_id,
        'group_info' => array('default_group' => NULL),
      );
      $filter->init($this->view, $options);
      $filter->op_equal();
    }
  }
}
