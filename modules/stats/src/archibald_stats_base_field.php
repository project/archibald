<?php

/**
 * @file
 * Contains archibald_stats_base_field.
 */

class archibald_stats_base_field extends views_handler_field {

  /**
   * @{inheritdoc}
   */
  function add_additional_fields($fields = NULL) {
    foreach ($fields as $field) {
      $this->aliases[$field] = $this->query->add_field($this->table, $field);
    }
  }

  /**
   * @{inheritdoc}
   * @noop
   */
  function click_sort($order) { }

  /**
   * @{inheritdoc}
   */
  function query() {
    $this->field_alias = $this->query->add_field($this->table, $this->real_field);

    // Add in additional fields.
    if (!empty($this->additional_fields)) {
      $this->add_additional_fields($this->additional_fields);
    }
  }
}
