<?php

/**
 * @file
 * Contains archibald_stats_partner_filter.
 */

class archibald_stats_partner_filter extends views_handler_filter_in_operator {

  /**
   * @{inheritdoc}
   */
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t("Partners");
      $options = array();
      foreach(archibald_partner_load_all() as $partner) {
        $options[$partner->id] = check_plain($partner->name);
      }
      $this->value_options = $options;
    }
  }

  /**
   * @{inheritdoc}
   */
  function operators() {
    $operators = parent::operators();
    // Only allow the "in" operator.
    return array('in' => $operators['in']);
  }

  /**
   * @{inheritdoc}
   */
  function op_simple() {
    if (empty($this->value)) {
      return;
    }
    $this->query->add_parameter('partner_id', array_values($this->value));
  }
}