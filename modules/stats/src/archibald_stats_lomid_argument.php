<?php

/**
 * @file
 * Contains archibald_stats_lomid_argument.
 */

class archibald_stats_lomid_argument extends views_handler_argument {

  /**
   * @{inheritdoc}
   */
  function query($group_by = FALSE) {
    // We basically add the same filter as when using the
    // archibald_stats_text_filter filter. Construct our filter, and activate
    // the op_equal() operation.
    $filter = new archibald_stats_text_filter();
    $options = $this->options + array(
      'operator' => '=',
      'value' => $this->argument,
      'group_info' => array('default_group' => NULL),
    );
    $filter->init($this->view, $options);
    $filter->field = $this->real_field;
    $filter->op_equal($this->real_field);
  }
}
