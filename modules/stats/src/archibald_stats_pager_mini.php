<?php

/**
 * @file
 * Contains archibald_stats_pager_mini.
 */

class archibald_stats_pager_mini extends views_plugin_pager_mini {

  /**
   * @{inheritdoc}
   */
  function query() {
    // Prevent Notices.
    $this->options['offset'] = 0;
    return parent::query();
  }

  /**
   * @{inheritdoc}
   */
  function summary_title() {
    if (!empty($this->options['offset'])) {
      return format_plural($this->options['items_per_page'], 'Archibald Stats pager, @count item, skip @skip', 'Archibald Stats pager, @count items, skip @skip', array('@count' => $this->options['items_per_page'], '@skip' => $this->options['offset']));
    }
    return format_plural($this->options['items_per_page'], 'Archibald Stats pager, @count item', 'Archibald Stats pager, @count items', array('@count' => $this->options['items_per_page']));
  }

  /**
   * @{inheritdoc}
   */
  function option_definition() {
    $options = parent::option_definition();
    unset($options['offset']);
    unset($options['total_pages']);
    return $options;
  }

  /**
   * @{inheritdoc}
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    unset($form['offset']);
    unset($form['total_pages']);
  }

  /**
   * @{inheritdoc}
   */
  function render($input) {
    $pager_theme = views_theme_functions('archibald_stats_pager_mini', $this->view, $this->display);

    // The 1, 3 index are correct.
    // @see theme_pager().
    $tags = array(
      1 => $this->options['tags']['previous'],
      3 => $this->options['tags']['next'],
    );

    return theme($pager_theme, array(
      'tags' => $tags,
      'element' => $this->get_pager_id(),
      'parameters' => $input,
    ));
  }
}
