<?php

/**
 * @file
 * Contains archibald_stats_lomid_filter.
 */

class archibald_stats_lomid_filter extends views_handler_filter_string {

  /**
   * @{inheritdoc}
   */
  function operators() {
    $operators = parent::operators();
    // Only allow the "=" operator.
    return array(
      '=' => $operators['='],
    );
  }

  /**
   * @{inheritdoc}
   */
  function op_equal() {
    if (empty($this->value)) {
      return;
    }
    $this->query->add_parameter('lom_id', $this->value);
  }

}
