<?php

/**
 * @file
 * Contains archibald_stats_partner_field.
 */

class archibald_stats_partner_field extends archibald_stats_base_field {

  /**
   * @{inheritdoc}
   */
  function render($values) {
    $format = $this->options['partner_format'];

    if ($format == 'id') {
      return check_plain($values->partner);
    }
    else {
      $partner = archibald_partner_load($values->partner);
      switch ($format) {
        case 'label':
          return check_plain($partner->name);
        case 'username':
          return check_plain($partner->partner_username[LANGUAGE_NONE][0]['value']);
        case 'display_name':
          return check_plain($partner->partner_display_name[LANGUAGE_NONE][0]['value']);
      }
    }
  }

  /**
   * @{inheritdoc}
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['partner_format'] = array('default' => 'id');
    return $options;
  }

  /**
   * @{inheritdoc}
   */
  function options_form(&$form, &$form_state) {
    $form['partner_format'] = array(
      '#type' => 'radios',
      '#title' => t("Partner display"),
      '#options' => array(
        'id' => t("Partner ID (raw)"),
        'label' => t("Partner label"),
        'username' => t("Partner username"),
        'display_name' => t("Partner display name"),
      ),
      '#default_value' => isset($this->options['partner_format']) ? $this->options['partner_format'] : 'id',
    );
    parent::options_form($form, $form_state);
  }
}
