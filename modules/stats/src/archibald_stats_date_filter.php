<?php

/**
 * @file
 * Contains archibald_stats_date_filter.
 */

class archibald_stats_date_filter extends views_handler_filter_date {

  /**
   * @{inheritdoc}
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['operator'] = array('default' => 'between');
    return $options;
  }

  /**
   * @{inheritdoc}
   */
  function operators() {
    $operators = parent::operators();
    // Only allow certain operators.
    return array(
      '<' => array('title' => t("Before (max 3 years back)")) + $operators['<'],
      '>' => array('title' => t("After (until today)")) + $operators['>'],
      'between' => $operators['between'],
    );
  }

  /**
   * @{inheritdoc}
   */
  function op_between($field) {
    $this->query->add_parameter('from', intval(strtotime($this->value['min'])));
    $this->query->add_parameter('to', intval(strtotime($this->value['max'])));
  }

  /**
   * @{inheritdoc}
   */
  function op_simple($field) {
    switch ($this->options['operator']) {
      case '<':
        $this->query->add_parameter('from', intval(strtotime('-3 years', strtotime($this->value['value']))));
        $this->query->add_parameter('to', intval(strtotime($this->value['value'])));
        break;

      case '>':
        $this->query->add_parameter('from', intval(strtotime($this->value['value'])));
        $this->query->add_parameter('to', time());
        break;
    }
  }

  /**
   * @{inheritdoc}
   */
  function query() {
    $field = "$this->table_alias.$this->real_field";

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($field);
    }
  }

}
