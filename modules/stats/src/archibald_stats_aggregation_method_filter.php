<?php

/**
 * @file
 * Contains archibald_stats_aggregation_method_filter.
 */

class archibald_stats_aggregation_method_filter extends views_handler_filter_in_operator {

  /**
   * @{inheritdoc}
   */
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t("Aggregate by");
      $options = array(
        'day' => t("Day"),
        'month' => t("Month"),
        'year' => t("Year"),
      );
      $this->value_options = $options;
    }
  }

  /**
   * @{inheritdoc}
   */
  function operators() {
    $operators = parent::operators();
    // Only allow the "in" operator.
    return array('in' => $operators['in']);
  }

  /**
   * @{inheritdoc}
   */
  function op_simple() {
    if (empty($this->value)) {
      return;
    }
    $this->query->add_parameter('aggregation_method', reset($this->value));
  }
}