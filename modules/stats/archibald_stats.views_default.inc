<?php

/**
 * @file
 * Default views for the Archibald Statistics module.
 */

/**
 * Implements hook_views_default_views().
 */
function archibald_stats_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'archibald_stats';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'archibald_stats';
  $view->human_name = 'Archibald statistics';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Statistics';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access archibald_stats statistics';
  $handler->display->display_options['cache']['type'] = 'views_custom_multi_cache';
  $handler->display->display_options['cache']['cache_time'] = '1800';
  $handler->display->display_options['cache']['cache_time_custom'] = '0';
  $handler->display->display_options['cache']['per_role'] = '1';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'archibald_stats_pager_mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_aggregator';
  $handler->display->display_options['style_options']['columns'] = array(
    'date' => 'date',
    'views' => 'views',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'date' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
      'has_aggr' => 0,
      'aggr' => array(
        'views_aggregator_first' => 'views_aggregator_first',
      ),
      'aggr_par' => '',
      'has_aggr_column' => 0,
      'aggr_column' => 'views_aggregator_sum',
      'aggr_par_column' => '',
    ),
    'views' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
      'has_aggr' => 0,
      'aggr' => array(
        'views_aggregator_first' => 'views_aggregator_first',
      ),
      'aggr_par' => '',
      'has_aggr_column' => 1,
      'aggr_column' => 'views_aggregator_sum',
      'aggr_par_column' => '',
    ),
  );
  $handler->display->display_options['style_options']['column_aggregation']['totals_per_page'] = '1';
  $handler->display->display_options['style_options']['column_aggregation']['totals_row_position'] = array(
    1 => '1',
    2 => '2',
  );
  $handler->display->display_options['style_options']['column_aggregation']['precision'] = '0';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No results to display.';
  $handler->display->display_options['empty']['area']['format'] = 'plain_text';
  /* Field: Archibald statistics: Description title */
  $handler->display->display_options['fields']['node_title']['id'] = 'node_title';
  $handler->display->display_options['fields']['node_title']['table'] = 'archibald_stats';
  $handler->display->display_options['fields']['node_title']['field'] = 'node_title';
  $handler->display->display_options['fields']['node_title']['label'] = '';
  $handler->display->display_options['fields']['node_title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['node_title']['element_label_colon'] = FALSE;
  /* Field: Archibald statistics: LOM ID */
  $handler->display->display_options['fields']['lomId']['id'] = 'lomId';
  $handler->display->display_options['fields']['lomId']['table'] = 'archibald_stats';
  $handler->display->display_options['fields']['lomId']['field'] = 'lomId';
  $handler->display->display_options['fields']['lomId']['label'] = '';
  $handler->display->display_options['fields']['lomId']['exclude'] = TRUE;
  $handler->display->display_options['fields']['lomId']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Title';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[node_title]<br />[lomId]';
  /* Field: Archibald statistics: Date */
  $handler->display->display_options['fields']['date']['id'] = 'date';
  $handler->display->display_options['fields']['date']['table'] = 'archibald_stats';
  $handler->display->display_options['fields']['date']['field'] = 'date';
  $handler->display->display_options['fields']['date']['date_format'] = 'custom';
  $handler->display->display_options['fields']['date']['custom_date_format'] = 'd/m/Y';
  $handler->display->display_options['fields']['date']['second_date_format'] = 'long';
  /* Field: Archibald statistics: View count */
  $handler->display->display_options['fields']['views']['id'] = 'views';
  $handler->display->display_options['fields']['views']['table'] = 'archibald_stats';
  $handler->display->display_options['fields']['views']['field'] = 'views';
  /* Filter criterion: Archibald statistics: From/to dates */
  $handler->display->display_options['filters']['from_to']['id'] = 'from_to';
  $handler->display->display_options['filters']['from_to']['table'] = 'archibald_stats';
  $handler->display->display_options['filters']['from_to']['field'] = 'from_to';
  $handler->display->display_options['filters']['from_to']['exposed'] = TRUE;
  $handler->display->display_options['filters']['from_to']['expose']['operator_id'] = 'from_to_op';
  $handler->display->display_options['filters']['from_to']['expose']['label'] = 'From';
  $handler->display->display_options['filters']['from_to']['expose']['description'] = 'Filter statistics by date.';
  $handler->display->display_options['filters']['from_to']['expose']['operator'] = 'from_to_op';
  $handler->display->display_options['filters']['from_to']['expose']['identifier'] = 'from_to';
  $handler->display->display_options['filters']['from_to']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['from_to']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );
  /* Filter criterion: Archibald statistics: Partner */
  $handler->display->display_options['filters']['partner']['id'] = 'partner';
  $handler->display->display_options['filters']['partner']['table'] = 'archibald_stats';
  $handler->display->display_options['filters']['partner']['field'] = 'partner';
  $handler->display->display_options['filters']['partner']['exposed'] = TRUE;
  $handler->display->display_options['filters']['partner']['expose']['operator_id'] = 'partner_op';
  $handler->display->display_options['filters']['partner']['expose']['label'] = 'Partner';
  $handler->display->display_options['filters']['partner']['expose']['operator'] = 'partner_op';
  $handler->display->display_options['filters']['partner']['expose']['identifier'] = 'partner';
  $handler->display->display_options['filters']['partner']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['partner']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );
  /* Filter criterion: Archibald statistics: Aggregation method */
  $handler->display->display_options['filters']['aggregation_method']['id'] = 'aggregation_method';
  $handler->display->display_options['filters']['aggregation_method']['table'] = 'archibald_stats';
  $handler->display->display_options['filters']['aggregation_method']['field'] = 'aggregation_method';
  $handler->display->display_options['filters']['aggregation_method']['value'] = array(
    'day' => 'day',
    'month' => 'month',
    'year' => 'year',
  );
  $handler->display->display_options['filters']['aggregation_method']['exposed'] = TRUE;
  $handler->display->display_options['filters']['aggregation_method']['expose']['operator_id'] = 'aggregation_method_op';
  $handler->display->display_options['filters']['aggregation_method']['expose']['label'] = 'Aggregation by';
  $handler->display->display_options['filters']['aggregation_method']['expose']['operator'] = 'aggregation_method_op';
  $handler->display->display_options['filters']['aggregation_method']['expose']['identifier'] = 'aggregation_method';
  $handler->display->display_options['filters']['aggregation_method']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['aggregation_method']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );

  /* Display: All description stats */
  $handler = $view->new_display('page', 'All description stats', 'all');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Descriptions';
  $handler->display->display_options['path'] = 'admin/archibald/description/statistics';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Statistics';
  $handler->display->display_options['menu']['weight'] = '100';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Node tab */
  $handler = $view->new_display('page', 'Node tab', 'page_1');
  $handler->display->display_options['defaults']['query'] = FALSE;
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Archibald statistics: Date */
  $handler->display->display_options['fields']['date']['id'] = 'date';
  $handler->display->display_options['fields']['date']['table'] = 'archibald_stats';
  $handler->display->display_options['fields']['date']['field'] = 'date';
  $handler->display->display_options['fields']['date']['date_format'] = 'custom';
  $handler->display->display_options['fields']['date']['custom_date_format'] = 'd/m/Y';
  $handler->display->display_options['fields']['date']['second_date_format'] = 'long';
  /* Field: Archibald statistics: View count */
  $handler->display->display_options['fields']['views']['id'] = 'views';
  $handler->display->display_options['fields']['views']['table'] = 'archibald_stats';
  $handler->display->display_options['fields']['views']['field'] = 'views';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Archibald statistics: Description NID */
  $handler->display->display_options['arguments']['node_nid']['id'] = 'node_nid';
  $handler->display->display_options['arguments']['node_nid']['table'] = 'archibald_stats';
  $handler->display->display_options['arguments']['node_nid']['field'] = 'node_nid';
  $handler->display->display_options['arguments']['node_nid']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['node_nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['node_nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['node_nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['node_nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['node_nid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['node_nid']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['node_nid']['validate_options']['types'] = array(
    'archibald_lomch_description' => 'archibald_lomch_description',
  );
  $handler->display->display_options['arguments']['node_nid']['validate_options']['access_op'] = 'update';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Archibald statistics: From/to dates */
  $handler->display->display_options['filters']['from_to']['id'] = 'from_to';
  $handler->display->display_options['filters']['from_to']['table'] = 'archibald_stats';
  $handler->display->display_options['filters']['from_to']['field'] = 'from_to';
  $handler->display->display_options['filters']['from_to']['exposed'] = TRUE;
  $handler->display->display_options['filters']['from_to']['expose']['operator_id'] = 'from_to_op';
  $handler->display->display_options['filters']['from_to']['expose']['label'] = 'From';
  $handler->display->display_options['filters']['from_to']['expose']['description'] = 'Filter statistics by date.';
  $handler->display->display_options['filters']['from_to']['expose']['operator'] = 'from_to_op';
  $handler->display->display_options['filters']['from_to']['expose']['identifier'] = 'from_to';
  $handler->display->display_options['filters']['from_to']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );
  /* Filter criterion: Archibald statistics: Aggregation method */
  $handler->display->display_options['filters']['aggregation_method']['id'] = 'aggregation_method';
  $handler->display->display_options['filters']['aggregation_method']['table'] = 'archibald_stats';
  $handler->display->display_options['filters']['aggregation_method']['field'] = 'aggregation_method';
  $handler->display->display_options['filters']['aggregation_method']['value'] = array(
    'day' => 'day',
    'month' => 'month',
    'year' => 'year',
  );
  $handler->display->display_options['filters']['aggregation_method']['exposed'] = TRUE;
  $handler->display->display_options['filters']['aggregation_method']['expose']['operator_id'] = 'aggregation_method_op';
  $handler->display->display_options['filters']['aggregation_method']['expose']['label'] = 'Aggregate by';
  $handler->display->display_options['filters']['aggregation_method']['expose']['operator'] = 'aggregation_method_op';
  $handler->display->display_options['filters']['aggregation_method']['expose']['identifier'] = 'aggregation_method';
  $handler->display->display_options['filters']['aggregation_method']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['aggregation_method']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );
  $handler->display->display_options['path'] = 'node/%/statistics';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Statistics';
  $handler->display->display_options['menu']['weight'] = '200';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['archibald_stats'] = array(
    t('Master'),
    t('Statistics'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('No results to display.'),
    t('Title'),
    t('[node_title]<br />[lomId]'),
    t('Date'),
    t('View count'),
    t('From'),
    t('Filter statistics by date.'),
    t('Partner'),
    t('Aggregation by'),
    t('All description stats'),
    t('Descriptions'),
    t('Node tab'),
    t('All'),
    t('Aggregate by'),
  );

  $export['archibald_stats'] = $view;

  return $export;
}
