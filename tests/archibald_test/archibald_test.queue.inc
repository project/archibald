<?php

/**
 * @file
 * Functions for testing the Archibald queue logic.
 */

/**
 * Queue callback.
 */
function archibald_test_queue_callback() {
  variable_set('__archibald_test_queue_callback', func_get_args());
}
