Reporting bugs using Selenium
=============================

<div style="display:none">*(Note: this document is in Markdown. If you are viewing this with your browser, you may want to consider installing a plugin, like [Markdown Viewer](https://addons.mozilla.org/en-US/firefox/addon/markdown-viewer/) for Firefox)*.</div>

When reporting a bug, it may be useful to attach a *failing* Selenium test. This test can be used by the developer to make sure the software is correctly fixed. Furthermore, this *failing* test can then be added to the collection of *regression tests* that ship with Archibald.

Why attach a test instead of describing the bug in an email?
------------------------------------------------------------

Usually, a *great* bug report provides detailed explanations of what the bug is, how it was triggered, and steps to reproduce it. This greatly speeds up the correction time for the developers.

One way to do this is to provide a step-by-step guide. For example:

1. Log in as an admin
2. Create a new description with ... 

(...many specific instructions later...)

21. Go to *Pending descriptions*
22. Click on *Validate*
23. Expected result: the description is valid. But it isn't!

This is a great starting point, but it's a bit tiresome for you to write and for developers to read. But there's a better way:

1. Open Firefox and navigate to your site
2. Log in first (otherwise your Selenium test will contain your username and password...)
3. Start the Selenium IDE (see [`README.md`](README.md) for more information)
4. Hit the *Record* button ![Record icon](img/record-btn.png)

Now, instead of *explaining* all the steps and writing them down, actually perform them in your browser, all the way to when the bug appears. Once you have triggered the bug, stop recording.

This is a *huge* help for a developer (and a time saver for yourself). Use the *Save Test Case As...* option from the top menu to save the test somewhere on your computer (on Windows: you will have to put the file extension manually, like `[file name].XML`). You can send this test file along with a short description of what *should* happen to the developers. The developer can now run the test as well, and exactly see how the bug is produced.

How to produce a *failing test*
-------------------------------

But, there's an even better way: produce a *failing* test. The test we created above will "succeed", because Selenium doesn't know about the bug. You must alter the test in order for it to *fail*. If the test fails, the developer will clearly see what needs to be done to correct the bug. Even better, the developer can run the test over and over again: as long as it fails, the bug is still there. As soon as it passes, the bug is fixed. This is what we call a *regression test*.

Let's take our example from above again. Let's say the validation logic has a bug. You expect a description to be valid, but Archibald says it isn't.

In your Selenium IDE window, you should see something like this:

![Initial Selenium test](img/test-case-1.png)

We need to change the last steps, in order for the test to *fail*. In our validation example from before, we can note 2 things that are incorrect:

1. There should *not* be a message saying "[Title] does not validate"
2. There *should* be a message saying "[Title] is valid"

What we can do is add 2 additional tests:

1. Check the text "[Title] does not validate" does *not* appear on the page
2. Check the text "[Title] is valid" *does* appear on the page

In order to do this, click on the first empty line *after* the last instruction in the Selenium window:

![Select the first empty line](img/test-case-add-1.png)

Now type the following in the form underneath:

* in the *Command* field, write `verifyNotText`
* in the *Target* field, write `css=body`
* in the *Value* field, write `glob:*does not validate*`

Like so:

![Add first failing test](img/test-case-add-2.png)

The `verifyNotText` command checks a certain text is not present on the page.

The `css=body` instruction is a very cryptic way to say "look on the entire page".

The `glob:` prefix tells Selenium you are looking for a few words, but that there can be many other words on the page. If you would simply write `does not validate` instead of `glob:*does not validate*`, it would expect the page to *only* contain the text `does not validate`. The `*` (asterisks) before and after the searched words show that there can be anything before and after.

Let's add our second test, which checks if "[Title] is valid" is present on the page. Click on the first empty line after the one we just added, and type:

* in the *Command* field, write `verifyText`
* in the *Target* field, write `css=body`
* in the *Value* field, write `glob:*is valid*`

Like so:

![Add second failing test](img/test-case-add-3.png)

We use the same logic, but instead of the `verifyNotText` command, we use `verifyText` (which does the opposite).

Now, if we run our Selenium test (![Run test button](img/play-btn.png)), it should go through all the steps again, but this time it will fail:

![Correctly failing Selenium test](img/test-case-2.png)

This Selenium test is even more useful to the developer, as it can be used to check the bug is indeed fixed, and it can be added to the list of *regression tests*, so we can ensure this bug never happens again. Just as before, use *Save Test Case As...* from the top menu, save the file, and send it to the developers.

Who will love you for it :-).

What are *regression tests*?
----------------------------

[Regression testing](https://en.wikipedia.org/wiki/Regression_testing) is a practice to make sure the software continues to behave as intended. For example, it is possible a bug is introduced at some point due to a human error (like a typo). Once fixed, however, there's no guarantee someone, in the future, will not make the same mistake *again*, which would make the bug appear again, although it was supposed to be fixed. This is where *regression tests* come into play.

A *regression test* tests the software for a very specific bug. Every time a new release is to be made, all tests (including regression tests) are run. If a regression test fails, it means a previously corrected bug has appeared again, and must be corrected before releasing the software.

It is a good practice, which allows releases to be less stressful, and reduce support hours :-).





