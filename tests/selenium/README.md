Archibald - Testing with Selenium
=================================

<div style="display:none">*(Note: this document is in Markdown. If you are viewing this with your browser, you may want to consider installing a plugin, like [Markdown Viewer](https://addons.mozilla.org/en-US/firefox/addon/markdown-viewer/) for Firefox)*.</div>

Archibald ships with a suite of Selenium tests, which allow us to assess more thoroughly the quality of our code before shipping new releases. These tests are located inside the `tests/selenium/` folder, and grouped by topic. They require the use of the Selenium IDE plugin to run.

[Read more about Selenium here](http://www.seleniumhq.org/). You can read more about [the Selenium IDE plugin for Firefox here](https://addons.mozilla.org/en-US/firefox/addon/selenium-ide/). It is also recommended to install the [Selenium Stored Variables plugin for Firefox](https://addons.mozilla.org/en-US/firefox/addon/stored-variables-viewer-seleni/). More information below.

In order to run these tests, we need to perform several manual tasks first.

Setting up
----------

The tests are meant to be run against a fresh install of Archibald. The site should be configured and ready to use, and it is better not to have any content lying around. Therefore, it is recommended to fully reinstall the test site. Tests *can* be run against a site with existing content, but if content remains from previous test runs, some actions may collide, and the test results may be incorrect.

### Reinstall Drupal

It is recommended to have the [Drupal Reset](https://www.drupal.org/project/drupal_reset) module for this. It greatly simplifies the re-initializing of the Drupal site.

1. Log in as User-1.
2. Make sure the *Drupal Reset* module is enabled. If not, enable it under `admin/modules`.
3. Go to `admin/config/development/drupal_reset` and select *Delete all database tables and files*. Click on *Reset this site*.

This will take you to the Drupal installation form. Select the *Minimal* profile, and proceed as you normally would. Once you are finished, go to `admin/modules` and enable the following modules:

* [Administration menu](https://www.drupal.org/project/admin_menu)
* One dependency manager. This means either:
  * [Composer Manager](https://www.drupal.org/project/composer_manager)
  * [Libraries](https://www.drupal.org/project/libraries) with [X Autoload](https://www.drupal.org/project/xautoload)
* Archibald

Whichever dependency manager is chosen, make sure the required libraries are available. See the [`README.txt`](../../README.txt) file shipping with Archibald for more information.

### Re-configure Archibald

When re-installing the site, Archibald will lose all its previous configuration.

#### Choose an environment

First, make sure the environment is connecting to either the demo (`https://demo-dsb-api.educa.ch/v2/`), test (`https://test-dsb-api.educa.ch/v2/`) or development (`https://dev-dsb-api.educa.ch/v2/`) environment. This is set at `admin/config/services/archibald`, in the *REST API settings* group.

**WARNING: MAKE SURE YOU DO THIS STEP CORRECTLY!** If the tests are run against the production environment, all sorts of bad things will happen!

#### Enable visual enhancement

If all Selenium tests are to be run, also enable the visual enhancements, in the *Visual settings* group, on the same `admin/config/services/archibald` page. If in doubt, just enable them.

#### Create a partner called *default*

You will have to create a partner (`admin/archibald/partner/add`), which has read and write access to the chosen API environment. Name it *default*, and make sure to check the *Make this partner the default* checkbox.

#### Re-import vocabularies

Go to the site status report (`admin/reports/status`), and click on the links to import vocabularies.

#### Check Archibald status

Go to the site status report (`admin/reports/status`), and make sure everything related to Archibald is OK.

### Re-configure the site appearance

The tests were recorded using *Seven* as the site's default theme. In order for the tests to work, you must change the site default theme to *Seven*. You can change this here: `admin/appearance`. Choose *Enable and set default* for *Seven*.

Running tests
-------------

### Pre-flight check list

* Drupal fully re-installed (optional, but recommended).
* Archibald, Administration Menu and one of the dependency managers (Composer 
    Manager or Libraries with X Autoload) enabled.
* Archibald API URL is set to demo, test or dev.
* Double check the above API URL!
* A partner called *default* was created (and marked as the default partner).
* Site default theme was set to *Seven*.

### Run tests

Tests need to be run using the Selenium IDE plugin for Firefox. Open Firefox, start the plugin, and load one of the test suites, located in `tests/selenium/`. Each suite is called `[name]-suite.xml`. Now navigate to your Archibald installation. The tests don't contain any credentials, so, before running them, you must first log in manually. Most tests are meant to be run as a site administrator (full privileges). Finally, in the Selenium IDE, change the *Base URL* to your site's URL, and start the tests.

#### Failing tests that should work

Sometimes, Selenium marks a test as failing, when it clearly should work. For example, it might complain it cannot find a form field, although it is clearly visible on screen. When this happens, try selecting a slower speed (top left corner of the Selenium plugin window).

#### File uploads and using the Stored Variables plugin

The [Stored Variables (Selenium IDE)](https://addons.mozilla.org/en-US/firefox/addon/stored-variables-viewer-seleni/) plugin for Firefox allows *variables* to be altered for a specific test run. This is especially useful for tests that need to upload files (like the `tests/selenium/test-cases/partners.xml` test). 

In Selenium, file uploads need to access a file on the computer at a pre-defined location. Obviously, this is not very portable. This is why Selenium exposes *variables*. Variables can hold information, that can be re-used throughout a test. A typical example is the privatekey upload for the *partners* test. Archibald ships with a dummy privatekey file, specifically for this test. However, Selenium doesn't know where Archibald is stored on the current computer. This is where variables come in. Variables always have a default value (in the case of the Archibald location, it is called `ArchibaldSourceLocation`, and its value is `/home/wadmiraal/Documents/educa/projects/dsb/archibald-3.x-src`). When running the *partners* test, Selenium will try to upload a privatekey located in `${ArchibaldSourceLocation}/tests/fixtures/privatekey.txt`. Obviously, this won't work on other computers. If you install the *Stored Variables* plugin, you can change the value of the `ArchibaldSourceLocation` variable for the duration of the test, and make it point to the location of Archibald on your own computer. By changing the location of Archibald to match that of your own computer, you can run the tests.

More information can be found [here](http://blog.reallysimplethoughts.com/my-selenium-ide-plugins/stored-variables/user-guide/).
