array(
  array (
    'field_name' => 'lomch_relations',
    'is_new' => '1',
    'archived' => '0',
    'lomch_relations_kind' =>
    array (
      'en' =>
      array (
        0 =>
        array (
          'value' => 'has_part_of',
        ),
      ),
    ),
    'lomch_relations_identifier' =>
    array (
      'en' =>
      array (
        0 =>
        array (
          'title' => '',
          'type' => 'URL',
          'value' => 'http://www.example.com',
        ),
      ),
    ),
    'lomch_relations_description' =>
    array (
      'en' =>
      array (
        0 =>
        array (
          'value' => 'Lorem ipsum dolor sit amet',
          'format' => NULL,
          'safe_value' => 'Lorem ipsum dolor sit amet',
        ),
      ),
    ),
  ),
  array (
    'field_name' => 'lomch_relations',
    'is_new' => '1',
    'archived' => '0',
    'lomch_relations_kind' =>
    array (
      'en' =>
      array (
        0 =>
        array (
          'value' => 'has_part_of',
        ),
      ),
    ),
    'lomch_relations_identifier' =>
    array (
      'en' =>
      array (
        0 =>
        array (
          'title' => '',
          'type' => 'DOI',
          'value' => '10.1000/xyz123',
        ),
      ),
    ),
    'lomch_relations_description' =>
    array (
      'en' =>
      array (
        0 =>
        array (
          'value' => 'This is the summer of our discontent',
          'format' => NULL,
          'safe_value' => 'This is the summer of our discontent',
        ),
      ),
    ),
  ),
  array (
    'field_name' => 'lomch_relations',
    'is_new' => '1',
    'archived' => '0',
    'lomch_relations_kind' =>
    array (
      'fr' =>
      array (
        0 =>
        array (
          'value' => 'has_part_of',
        ),
      ),
    ),
    'lomch_relations_identifier' =>
    array (
      'fr' =>
      array (
        0 =>
        array (
          'title' => '',
          'type' => 'URL',
          'value' => 'http://www.example.com',
        ),
      ),
    ),
    'lomch_relations_description' =>
    array (
      'fr' =>
      array (
        0 =>
        array (
          'value' => 'Supercalifragilisticexpialidocious',
          'format' => NULL,
          'safe_value' => 'Supercalifragilisticexpialidocious',
        ),
      ),
    ),
  ),
  array (
    'field_name' => 'lomch_relations',
    'is_new' => '1',
    'archived' => '0',
    'lomch_relations_kind' =>
    array (
      'fr' =>
      array (
        0 =>
        array (
          'value' => 'is_part_of',
        ),
      ),
    ),
    'lomch_relations_identifier' =>
    array (
      'fr' =>
      array (
        0 =>
        array (
          'title' => '',
          'type' => 'URL',
          'value' => 'http://www.google.com',
        ),
      ),
    ),
    'lomch_relations_description' =>
    array (
      'fr' =>
      array (
        0 =>
        array (
          'value' => 'Hello there, all you people',
          'format' => NULL,
          'safe_value' => 'Hello there, all you people',
        ),
      ),
    ),
  ),
)