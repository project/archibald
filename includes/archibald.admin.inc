<?php

/**
 * @file
 * Settings and administration callbacks for Archibald.
 */

/**
 * Module settings form callback.
 */
function archibald_admin_settings_form($form, $form_state) {
  // Make sure dependencies are loaded before configuring the module.
  if (
    !archibald_check_libraries(TRUE) ||
    !archibald_check_module_dependency(TRUE)
  ) {
    drupal_set_message(t("Please correct the dependency errors before configuring Archibald."), 'error');
    return array();
  }

  // API settings.
  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t("REST API settings"),
    '#collapsible' => TRUE,
    '#collapsed' =>
      variable_get('archibald_api_version', NULL) &&
      variable_get('archibald_api_url', NULL),
  );

  $form['api']['archibald_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t("API URL"),
    '#default_value' => variable_get('archibald_api_url', NULL),
    '#required' => TRUE,
  );

  $form['api']['archibald_api_version'] = array(
    '#type' => 'radios',
    '#title' => t("API version"),
    '#options' => array(
      2 => '2.x',
    ),
    '#default_value' => variable_get('archibald_api_version', NULL),
    '#required' => TRUE,
  );

  // Content settings.
  $form['content'] = array(
    '#type' => 'fieldset',
    '#title' => t("Content settings"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['content']['archibald_host_images_on_api'] = array(
    '#type' => 'checkbox',
    '#title' => t("Host the images on the API server"),
    '#description' => t("When publishing a description that contains a preview image, the image can either be hosted locally, or uploaded to the dsb API server. If you wish to have complete control over your images, you might want to host them locally. If you are more concerned with performance, or if Archibald is sitting behind a firewall, you might want to upload them and let the dsb API server handle them. Note that, once an image has been uploaded, <em>you cannot remove it from the dsb API server!</em>"),
    '#default_value' => variable_get('archibald_host_images_on_api', 0),
  );

  $form['content']['archibald_show_link_to_dsb_portal'] = array(
    '#type' => 'checkbox',
    '#title' => t("Show 'view on portal' preview links"),
    '#description' => t("The description preview Archibald provides is not an accurate representation of what it will look like on portals. By selecting this option, it becomes possible to set a URL to a portal. Links will then appear for published descriptions, so editors can see the description in a real-life context."),
    '#default_value' => variable_get('archibald_show_link_to_dsb_portal', 0),
  );

  $form['content']['archibald_dsb_portal_url'] = array(
    '#type' => 'textfield',
    '#title' => t("URL for 'view on portal' preview links"),
    '#description' => t("The URL must contain a <em>[ID]</em> token, and a protocol. E.g.: <em>http://portal.example.com/dsb-portal/description/[ID]</em>."),
    '#default_value' => variable_get('archibald_dsb_portal_url', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="archibald_show_link_to_dsb_portal"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Visual settings.
  $form['theme'] = array(
    '#type' => 'fieldset',
    '#title' => t("Visual settings"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['theme']['archibald_enhance_admin_themes'] = array(
    '#type' => 'checkbox',
    '#title' => t("Enhance Seven or Adminimal themes"),
    '#description' => t("Archibald ships with specific CSS styles that can enhance the displays when using Seven or Adminimal as the administration theme. Enabling this option will activate these styles."),
    '#default_value' => variable_get('archibald_enhance_admin_themes', 0),
  );

  $form['theme']['archibald_activate_collapsible_vtabs'] = array(
    '#type' => 'checkbox',
    '#title' => t("Activate collapsible vertical tabs"),
    '#description' => t("When this option is activated, users can collapse the vertical tabs on the left (or right for RTL sites) to have more screen space available when creating or editing <em>Descriptions</em>. Activating this option will display a <em>Collapse column</em> link above the vertical tabs, which can toggle their display."),
    '#default_value' => variable_get('archibald_activate_collapsible_vtabs', 0),
  );

  $form['theme']['archibald_activate_footer_links'] = array(
    '#type' => 'checkbox',
    '#title' => t("Activate vertical tab footer links"),
    '#description' => t("When creating or editing <em>Descriptions</em>, users navigate through the form sections using the vertical tabs on the left (or right for RTL sites). This will display <em>previous, next</em> links at the bottom of each section, to facilitate the passing from one section to another."),
    '#default_value' => variable_get('archibald_activate_footer_links', 0),
  );

  $form['theme']['archibald_activate_sticky_vtabs'] = array(
    '#type' => 'checkbox',
    '#title' => t("Activate sticky vertical tabs"),
    '#description' => t("When creating or editing <em>Descriptions</em>, this will make the vertical tabs stick to the top of the screen when scrolling down."),
    '#default_value' => variable_get('archibald_activate_sticky_vtabs', 0),
  );

  return system_settings_form($form);
}

/**
 * Validation callback for archibald_admin_settings_form().
 */
function archibald_admin_settings_form_validate($form, &$form_state) {
  // If the archibald_show_link_to_dsb_portal option is selected, we must
  // validate archibald_dsb_portal_url. archibald_dsb_portal_url also becomes
  // a required value.
  if (!empty($form_state['values']['archibald_show_link_to_dsb_portal'])) {
    if (empty($form_state['values']['archibald_dsb_portal_url'])) {
      form_set_error('archibald_dsb_portal_url', t("If you choose to show 'view on portal' preview links, you must provide a valid URL."));
    }
    elseif (!valid_url($form_state['values']['archibald_dsb_portal_url'], TRUE)) {
      form_set_error('archibald_dsb_portal_url', t("Please provide a valid, absolute URL (must contain the protocol, like 'http://')."));
    }
    elseif (!preg_match('/\[ID\]/', $form_state['values']['archibald_dsb_portal_url'])) {
      form_set_error('archibald_dsb_portal_url', t("The URL must contain a '[ID]' token."));
    }
  }
}

/**
 * Batch URL trigger for importing vocabularies.
 *
 * @see archibald_batch_import_vocabularies()
 * @see archibald_batch_import_vocabularies_finished()
 */
function archibald_batch_import_vocabularies_trigger($vocabulary) {
  $path = drupal_get_path('module', 'archibald');
  $batch = array(
    'title' => t("Import vocabularies"),
    'operations' => array(
      array('archibald_batch_import_vocabularies', array($vocabulary))
    ),
    'file' => "$path/includes/archibald.admin.inc",
  );

  batch_set($batch);

  // Redirect to the status page when finished.
  batch_process('admin/reports/status');
}

/**
 * Batch operation callback.
 *
 * This callback is used to import vocabularies necessary for Archibald in a
 * batch.
 *
 * @see archibald_drush_create_vocabularies()
 */
function archibald_batch_import_vocabularies($vocabulary, &$context) {
  module_load_include('inc', 'archibald', 'drush/archibald.drush');
  archibald_drush_create_vocabularies($vocabulary, TRUE);

  // Check if the import succeeded.
  if (variable_get("archibald_{$vocabulary}_vocabulary_created", 0)) {
    drupal_set_message(t("Successfully imported %type vocabulary.", array(
      '%type' => $vocabulary,
    )));
  }
  else {
    drupal_set_message(t("Something went wrong when importing the %type vocabulary.", array(
      '%type' => $vocabulary,
    )), 'error');
  }

  $context['finished'] = 1;
}
