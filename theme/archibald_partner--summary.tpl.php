<?php
/**
 * @file
 * Theme implementation to display summary information for an archibald_partner.
 *
 * Available variables:
 * - $entity: the partner entity to display.
 * - $id: the (sanitized) partner ID.
 * - $name: the (sanitized) partner name (as stored in the system; usually
 *   the same as "$display_name ($username)".
 * - $display_name: the (sanitized) display name of the partner.
 * - $username: the (sanitized) username of the partner.
 * - $organization: the (sanitized) organization of the partner.
 * - $country: the (sanitized) country of the partner.
 * - $zip: the (sanitized) zip of the partner's address.
 * - $city: the (sanitized) city of the partner's address.
 * - $address: the (sanitized) address of the partner (street name and number).
 * - $add_address: the (sanitized) additional address information of the
 *   partner, if available.
 * - $full_address: the (sanitized) fully formatted address, containing all the
 *   above data (Swiss formatting).
 * - $url: the URL property of the partner (not sanitized).
 * - $logo_uri: the URI of the partner logo (if any).
 * - $logo: a fully themed logo (if any), ready for use.
 *
 * @see archibald_preprocess_archibald_partner()
 *
 * @ingroup themeable
 */
?>
<div class="archibald-partner archibald-partner--summary">
  <?php if (!empty($logo)): ?>
    <div class="archibald-partner__logo">
      <?php print $logo; ?>
    </div>
  <?php endif; ?>

  <div class="archibald-partner__name">
    <?php print $display_name; ?>
    <?php if (user_access('administer archibald_partners')): ?>
      (<?php print l($username, "admin/archibald/partner/manage/$id"); ?>)
    <?php else: ?>
      (<?php print $username; ?>)
    <?php endif; ?>
  </div>
</div>
