<?php

/**
 * @file
 * Preprocessors, helper functions, and theme callbacks.
 */

/**
 * Custom implementation of theme_form_required_marker().
 *
 * @see archibald_theme_registry_alter()
 */
function theme_archibald_form_required_marker($vars) {
  $element = $vars['element'];
  if (isset($element['#required_type']) && $element['#required_type'] != 'required') {
    $attributes = array(
      'class' => 'form-required ' . str_replace('_', '-', $element['#required_type']),
      'title' => $element['#required_type'] == 'archibald_lom_required' ?
        t("This field is required for a valid LOM-CH description.") :
        t("This field is recommended."),
    );
    return '<span' . drupal_attributes($attributes) . '>*</span>';
  }
  return theme_form_required_marker($vars);
}

/**
 * Custom implementation of theme_image_formatter().
 *
 * @see archibald_node_view()
 */
function theme_archibald_preview_image_formatter($vars) {
  $output = '<div class="archibald-preview-image">' . theme_image_formatter($vars);

  if (!empty($vars['item']['title'])) {
    $output .= '<div class="archibald-preview-image__copyright">';
    $output .= '&copy; ';
    $output .= check_plain($vars['item']['title']);
    $output .= '</div>';
  }

  return $output . '</div>';
}

/**
 * Implements hook_preprocess_archibald_partner().
 */
function archibald_preprocess_archibald_partner(&$vars, $theme_hook) {
  $partner = $vars['entity'];

  // Add CSS.
  drupal_add_css(drupal_get_path('module', 'archibald') . '/css/archibald.partner_entity_view.css');
  archibald_add_admin_theme_styles();

  // Prevent notices.
  $vars['classes_array'] = array();
  $vars['attributes_array'] = array();
  $vars['title_attributes_array'] = array();
  $vars['content_attributes_array'] = array();

  // Provide more variables to the templates.
  $vars['id'] = check_plain($partner->id);
  $vars['name'] = check_plain($partner->name);

  foreach (array(
    'display_name',
    'username',
    'organization',
    'country',
    'zip',
    'city',
    'address',
    'add_address',
  ) as $field) {
    $vars[$field] = !empty($partner->{"partner_$field"}[LANGUAGE_NONE][0]['value']) ?
      check_plain($partner->{"partner_$field"}[LANGUAGE_NONE][0]['value']) :
      '';
  }

  $vars['full_address'] = '';
  if (!empty($vars['address'])) {
    $vars['full_address'] .= $vars['address'];
    if (!empty($vars['add_address'])) {
      $vars['full_address'] .= ' ' . $vars['add_address'];
    }

    // If any field comes after this one, add a comma.
    if (!empty($vars['zip']) || !empty($vars['city']) || !empty($vars['country'])) {
      $vars['full_address'] .= ', ';
    }
  }
  if (!empty($vars['zip'])) {
    $vars['full_address'] .= $vars['zip'];
    if (!empty($vars['city'])) {
      $vars['full_address'] .= ', ' . $vars['city'];
    }

    // If a country comes after this, add a comma.
    if (!empty($vars['country'])) {
      $vars['full_address'] .= ', ';
    }
  }
  if (!empty($vars['country'])) {
    $vars['full_address'] .= $vars['country'];
  }

  // Provide the URL property raw.
  $vars['url'] = !empty($partner->partner_url[LANGUAGE_NONE][0]['value']) ?
    $partner->partner_url[LANGUAGE_NONE][0]['value'] :
    '';

  // Provide the logo, both as a URI and a theme image.
  if (!empty($partner->partner_logo[LANGUAGE_NONE][0]['fid'])) {
    $vars['logo_uri'] = $partner->partner_logo[LANGUAGE_NONE][0]['uri'];
    $vars['logo'] = theme('image_style', array(
      'style_name' => $vars['logo_image_style'],
      'path' => $vars['logo_uri'],
      'alt' => t("Logo for @name", array('@name' => $partner->name)),
    ));
  }
  else {
    $vars['logo_uri'] = $vars['logo'] = '';
  }
}
