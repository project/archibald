<?php
/**
 * @file
 * Theme implementation to display information for an archibald_partner.
 *
 * Available variables:
 * - $entity: the partner entity to display.
 * - $id: the (sanitized) partner ID.
 * - $name: the (sanitized) partner name (as stored in the system; usually
 *   the same as "$display_name ($username)".
 * - $display_name: the (sanitized) display name of the partner.
 * - $username: the (sanitized) username of the partner.
 * - $organization: the (sanitized) organization of the partner.
 * - $country: the (sanitized) country of the partner.
 * - $zip: the (sanitized) zip of the partner's address.
 * - $city: the (sanitized) city of the partner's address.
 * - $address: the (sanitized) address of the partner (street name and number).
 * - $add_address: the (sanitized) additional address information of the
 *   partner, if available.
 * - $full_address: the (sanitized) fully formatted address, containing all the
 *   above data (Swiss formatting).
 * - $url: the URL property of the partner (not sanitized).
 * - $logo_uri: the URI of the partner logo (if any).
 * - $logo: a fully themed logo (if any), ready for use.
 *
 * @see archibald_preprocess_archibald_partner()
 *
 * @ingroup themeable
 */
?>
<div class="archibald-partner archibald-partner--default">
  <div class="archibald-partner__logo">
    <?php if (!empty($logo)): ?>
      <?php print $logo; ?>
    <?php endif; ?>
  </div>

  <div class="archibald-partner__info">
    <div class="archibald-partner__info__field archibald-partner__info__field--name">
      <span class="archibald-partner__info__field__label"><?php print t("Name"); ?>:</span>
      <span class="archibald-partner__info__field__value"><?php print $display_name; ?> (<?php print $username; ?>)</span>
    </div>

    <?php if (!empty($organization)): ?>
      <div class="archibald-partner__info__field archibald-partner__info__field--organization">
        <span class="archibald-partner__info__field__label"><?php print t("Organization"); ?>:</span>
        <span class="archibald-partner__info__field__value"><?php print $organization; ?></span>
      </div>
    <?php endif; ?>

    <?php if (!empty($url)): ?>
      <div class="archibald-partner__info__field archibald-partner__info__field--url">
        <span class="archibald-partner__info__field__label"><?php print t("URL"); ?>:</span>
        <span class="archibald-partner__info__field__value"><?php print l(check_plain($url), $url, array('absolute' => TRUE)); ?></span>
      </div>
    <?php endif; ?>

    <?php if (!empty($full_address)): ?>
      <div class="archibald-partner__info__field archibald-partner__info__field--address">
        <span class="archibald-partner__info__field__label"><?php print t("Address"); ?>:</span>
        <span class="archibald-partner__info__field__value"><?php print $full_address; ?></span>
      </div>
    <?php endif; ?>
  </div>

  <?php if (user_access('administer archibald_partners')): ?>
    <div class="archibald-partner__operations">
      <?php print l(t("edit"), "admin/archibald/partner/manage/$id"); ?>
      <?php print l(t("delete"), "admin/archibald/partner/manage/$id/delete"); ?>
    </div>
  <?php endif; ?>
</div>
