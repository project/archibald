<?php

/**
 * @file
 * Devel generate callback for generating content for our custom field types.
 */

/**
 * Implements hook_devel_generate().
 */
function archibald_devel_generate($object, $field, $instance, $bundle) {
  $function = "_archibald_{$field['type']}_devel_generate";
  if (function_exists($function)) {
    if (field_behaviors_widget('multiple values', $instance) == FIELD_BEHAVIOR_CUSTOM) {
      return devel_generate_multiple($function, $object, $field, $instance, $bundle);
    }
    else {
      return $function($object, $field, $instance, $bundle);
    }
  }

  return array();
}

/**
 * Generate some random content for archibald_lomch_udi_identifier fields.
 *
 * @see archibald_devel_generate()
 */
function _archibald_archibald_lomch_udi_identifier_devel_generate($object, $field, $instance, $bundle) {
  $types = array('URL', 'DOI', 'ISBN');
  $value = array('type' => $types[array_rand($types)]);
  switch ($value['type']) {
    case 'URL':
      $domains = array_map('strtolower', explode(' ', devel_create_greeking(6, TRUE)));
      $tlds = array('com', 'net', 'org');
      $value['value'] = 'http://' . $domains[array_rand($domains)] . '.' . $tlds[array_rand($tlds)];
      break;

    case 'DOI':
      $numbers = array();
      foreach (array(4, 3) as $digits) {
        $numbers[] = rand(pow(10, $digits-1), pow(10, $digits)-1);
      }
      $value['value'] = "10.{$numbers[0]}/xyz{$numbers[1]}";
      break;

    case 'ISBN':
      $numbers = array();
      foreach (array(3, 1, 2, 6, 1) as $digits) {
        $numbers[] = rand(pow(10, $digits-1), pow(10, $digits)-1);
      }
      $value['value'] = implode('-', $numbers);
      break;
  }

  if ($instance['settings']['show_title']) {
    $value['title'] = devel_create_greeking(round(rand(5, 10)));
  }

  return $value;
}

/**
 * Generate some random content for archibald_lomch_ontology_vocabulary fields.
 *
 * @see archibald_devel_generate()
 */
function _archibald_archibald_lomch_ontology_vocabulary_devel_generate($object, $field, $instance, $bundle) {
  module_load_include('inc', 'archibald', 'includes/archibald.field');

  // Check if the connection with the ReST API is usable. If not, we cannot
  // generate values for Ontology Vocabulary fields.
  if (!archibald_check_module_usable()) {
    return array();
  }

  $list = array();
  $options = archibald_lomch_ontology_vocabulary_options_list($field, $instance, $bundle, $object);
  array_walk_recursive($options, function($item, $key) use(&$list) {
    $list[] = $key;
  });

  return array(
    'value' => $list[array_rand($list)],
  );
}
