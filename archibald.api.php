<?php

/**
 * @file
 * Archibald module API documentation.
 */

/**
 * @defgroup archibald_queue_api Archibald Queue API
 * @{
 * Archibald builds upon the @link queue Drupal Queue @endlink to queue items
 * for later execution. It allows logic to be processed later. For example,
 * re-publishing a node, which failed because of a network failure, is a typical
 * use-case within Archibald.
 *
 * Items are tagged with a specific group name. This name must respect a certain
 * logic. For example, queues for nodes are marked "node:{nid}:{action}". This
 * allows queues to be deleted by group. For example:
 * @code
 * archibald_clear_queue("node:23:publish");
 * @endcode
 * It is also possible to pass wildcards, like so:
 * @code
 * archibald_clear_queue("node:23:*");
 * @endcode
 * This tagging of queued items is especially important when queued items become
 * obsolete. For example, a queued re-publishing becomes obsolete when the node
 * is deleted. Upon deletion, the entire queue related to the deleted node is
 * cleared, but other pending items are remained intact.
 *
 * Queued items register a callback, which can live inside a different file, as
 * long as this file is relative to the archibald module directory, and is an
 * .inc file (see module_load_include()).
 *
 * @see queue
 * @}
 */

/**
 * @defgroup archibald_api Archibald
 * @{
 * Archibald allows Drupal sites to connect to the national catalog of the
 * Swiss Digital School Library. Communication with the national catalog happens
 * through a REST API. The module relies on the dsb Client PHP library for
 * communicating with this REST API. This library can be found here:
 * https://github.com/educach/dsb-client
 *
 * In order to use this module, users need to register with educa.ch to become
 * a content partner. Content partner receive a username (usually an email),
 * a private RSA key and a passphrase. This information can then be used by the
 * dsb Client library to communicate with the REST API.
 *
 * @see https://github.com/educach/dsb-client
 * @see https://dsb-api.educa.ch/latest/doc
 * @}
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Implements hook_archibald_language_fallback_list_alter().
 *
 * Some LOM-CH data fields are multilingual. By default, the current session
 * language will be searched for first, followed by:
 * - de
 * - fr
 * - it
 * - rm
 * - en
 *
 * It is possible to alter this list by implementing this hook.
 *
 * @param array &$languages
 *    The list of languages, in order of precedence.
 *
 * @ingroup archibald_api
 */
function hook_archibald_language_fallback_list_alter(&$languages) {
  $languages[] = 'ja';
}

/**
 * Implements hook_archibald_validation_report_alter().
 *
 * Alter the validation report.
 *
 * @param array &$report
 *    The report, keyed by field name. Each entry has the following keys:
 *    - errors: A list of errors. Each error is keyed by the "path" to the
 *      faulty field, and the value is the one returned by the API.
 *    - field_label: The human-readable name of the field that has an error.
 * @param array $context
 *    An array containing the following keys:
 *    - node: The node that was validated.
 *    - fields: The field definitions used to generate the report.
 *    - result: The raw result, as returned by the API.
 */
function hook_archibald_validation_report_alter(&$report, $context) {

}

/**
 * Implements hook_archibald_json_export_alter().
 *
 * In order to be sent to the dsb API, description data must be encoded to
 * JSON. Archibald will load the archibald_lomch_description node and map
 * its fields to properties on the $data object. The $data object maps closely
 * to the expected JSON structure, and uses stdClasses instead of associative
 * arrays.
 *
 * @param object $data
 *    The data that will eventually get exported to JSON. Alter this data
 *    structure to update existing keys or add new data.
 * @param object $node
 *    The description node.
 *
 * @ingroup archibald_api
 */
function hook_archibald_json_export_alter(&$data, $node) {
  $data->classification[] = $new_taxonpath;
}

/**
 * Implements hook_archibald_assign_to_partner_action().
 *
 * The archibald_assign_to_partner_action action was just invoked on the passed
 * node. At this point, it has not been saved yet, and can still be changed.
 *
 * @param object $node
 *    The node the action was invoked on.
 * @param array $context
 *    The context in which the action was invoked. Will contain at least the
 *    following key:
 *    - archibald_partner: The ID of the partner this node will get assigned to.
 */
function hook_archibald_assign_to_partner_action($node, $context) {

}

/**
 * Implements hook_archibald_assign_to_catalog_action().
 *
 * The archibald_assign_to_catalog_action action was just invoked on the passed
 * node. At this point, it has not been saved yet, and can still be changed.
 *
 * @param object $node
 *    The node the action was invoked on.
 * @param array $context
 *    The context in which the action was invoked. Will contain at least the
 *    following key:
 *    - archibald_catalog: The catalog(s) this node will be assigned to.
 */
function hook_archibald_assign_to_catalog_action($node, $context) {

}

/**
 * Implements hook_archibald_assign_to_user_action().
 *
 * The archibald_assign_to_user_action action was just invoked on the passed
 * node. At this point, it has not been saved yet, and can still be changed.
 *
 * @param object $node
 *    The node the action was invoked on.
 * @param array $context
 *    The context in which the action was invoked. Will contain at least the
 *    following key:
 *    - owner_uid: The user ID this node will be assigned to.
 */
function hook_archibald_assign_to_user_action($node, $context) {

}

/**
 * Implements hook_archibald_validate_action().
 *
 * The archibald_validate_action action was just invoked on the passed
 * node.
 *
 * @param object $node
 *    The node the action was invoked on.
 * @param array $result
 *    The validation result, as returned by the dsb REST API.
 * @param array $context
 *    The context in which the action was invoked. Usually this is an empty
 *    array.
 */
function hook_archibald_validate_action($node, $result, $context) {

}

/**
 * Implements hook_archibald_publish_action().
 *
 * The archibald_publish_action action was just invoked on the passed
 * node. At this point, the node has already been published.
 *
 * @param object $node
 *    The node the action was invoked on.
 * @param array $context
 *    The context in which the action was invoked. Usually this is an empty
 *    array.
 */
function hook_archibald_publish_action($node, $context) {

}

/**
 * Implements hook_archibald_republish_action().
 *
 * The archibald_republish_action action was just invoked on the passed
 * node. At this point, the node has already been republished.
 *
 * @param object $node
 *    The node the action was invoked on.
 * @param array $context
 *    The context in which the action was invoked. Usually this is an empty
 *    array.
 */
function hook_archibald_republish_action($node, $context) {

}

/**
 * Implements hook_archibald_publish_description().
 *
 * The node is being published. The $op variable specifies where we are in the
 * publication process.
 *
 * @param string $op
 *    The current operation. Can be one of the following:
 *    - "pre publish": The data is about to be published.
 *    - "post publish": The data was just published.
 *    - "publish error": The publishing failed due to an error.
 * @param object $data
 *    The data, which will get exported to JSON, and sent to the API.
 * @param object $node
 *    The node who's data is being published.
 */
function hook_archibald_publish_description($op, $data, $node) {

}

/**
 * Implements hook_archibald_unpublish_description().
 *
 * The node is being unpublished. The $op variable specifies where we are in the
 * publication process.
 *
 * @param string $op
 *    The current operation. Can be one of the following:
 *    - "pre unpublish": The data is about to be unpublished.
 *    - "post unpublish": The data was just unpublished.
 *    - "unpublish error": The unpublishing failed due to an error.
 * @param object $node
 *    The node who's data is being unpublished.
 */
function hook_archibald_unpublish_description($op, $node) {

}

/**
 * Implements hook_archibald_delete_description().
 *
 * The node is being deleted. The $op variable specifies where we are in the
 * publication process.
 *
 * @param string $op
 *    The current operation. Can be one of the following:
 *    - "pre delete": The data is about to be deleted.
 *    - "post delete": The data was just deleted.
 *    - "delete error": The deleting failed due to an error.
 * @param object $node
 *    The node who's data is being deleted.
 */
function hook_archibald_delete_description($op, $node) {

}

/**
 * Implements hook_archibald_vcard_access().
 *
 * Check if the user has access to this specific VCard. By default, users
 * with the 'view archibald_vcards' have access, but access can be restricted
 * by implementing this hook. If any module returns false, access will be
 * denied. Returning true has no effect if the user doesn't have either the
 * 'view archibald_vcards' or 'administer archibald_vcards' permission.
 *
 * It is not possible to deny access to users with the
 * 'administer archibald_vcards' permission.
 *
 * @param string $op
 *    "view", "create", "update" or "delete".
 * @param object $vcard
 *    The vcard. In case of "create", this will be null.
 * @param object $account
 *    The user to check against, if any.
 *
 * @return bool
 *    Return false to deny access. All other values (including true) will be
 *    ignored.
 */
function hook_archibald_vcard_access($op, $vcard = NULL, $account = NULL) {
  return FALSE;
}

/**
 * Implements hook_archibald_partner_access().
 *
 * Check if the user has access to this specific partner. By default, users
 * with the 'view archibald_partners' have access, but access can be restricted
 * by implementing this hook. If any module returns false, access will be
 * denied. Returning true has no effect if the user doesn't have either the
 * 'view archibald_partners' or 'administer archibald_partners' permission.
 *
 * It is not possible to deny access to users with the
 * 'administer archibald_partners' permission.
 *
 * @param string $op
 *    "view", "create", "update" or "delete".
 * @param object $partner
 *    The partner. In case of "create", this will be null.
 * @param object $account
 *    The user to check against, if any.
 *
 * @return bool
 *    Return false to deny access. All other values (including true) will be
 *    ignored.
 */
function hook_archibald_partner_access($op, $partner = NULL, $account = NULL) {
  return FALSE;
}


/**
 * @} End of "addtogroup hooks".
 */
