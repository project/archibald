=======================================================
Archibald - client module for managing LOM-CH resources
=======================================================

This module allows a site to manage LOM-CH resources, as well as publish them to the national catalog of the Swiss digital school library (a.k.a. "dsb", Digitale Schulbibliothek).

The module communicates with the official API, which can be found here: https://dsb-api.educa.ch/

A NOTE ON SECURITY : PLEASE READ
================================

Archibald has to store sensitive data, namely private RSA keys, as well as the passphrases to unlock these keys. For this, it requires a private file system. IT IS VERY IMPORTANT THIS FILE SYSTEM IS OUTSIDE THE WEBROOT! See the official Drupal documentation on private file systems for more information on how to set it up, as well as best practices. Furthermore, it is important that server permissions are setup correctly for this directory. IF YOU ARE HOSTING ARCHIBALD ON A SHARED HOSTING, CONSIDER MOVING TO A DEDICATED VPS! Some shared hosting providers don't properly secure data, meaning certain users can get access to files from other users, using CGI scripts, for example.

Dependency installation
=======================

There are 3 ways to install the module's dependencies.

* Using Drush and Composer (requires the Composer Manager module)
* Using Drush only (requires the Libraries API and X Autoload modules)
* Downloading third-party libraries "by hand" and uploading them to your server via FTP (requires the Libraries API and X Autoload modules)

You can choose either method, but you must choose at least one.


Using Composer, Drush and Composer Manager
------------------------------------------

First, install Drush and Composer on your host.

* http://docs.drush.org/en/master/install/
* https://getcomposer.org/doc/00-intro.md

Download and enable the Composer Manager (https://www.drupal.org/project/composer_manager) module. Change directory inside your Drupal site and call:

drush composer-json-rebuild

This will generate a composer.json file. By default, it is located in public://composer (usually, this maps to sites/default/files/composer). Now call:

drush composer-manager install

(Note: At the time of writing, this asks if you wish to install the composer-manager command. You must choose Yes, but this will fail with an error message. Not to worry, simply re-run the "drush composer-manager install" command, and it should work.)

Go to your site status report (admin/reports/status) and make sure Archibald has found all required libraries.


Using Drush only
----------------

First, install Drush on your host (http://docs.drush.org/en/master/install/).

Download and enable the Libraries API (https://www.drupal.org/project/libraries) and X Autoload (https://www.drupal.org/project/xautoload) modules. Change directory inside your Drupal site and call:

drush archibald-libraries-setup

This will put all libraries inside sites/all/libraries. If you wish to use another libraries folder, simply pass it to the command:

drush archibald-libraries-setup sites/example.com/libraries

Go to your site status report (admin/reports/status) and make sure Archibald has found all required libraries.


Downloading third-party libraries and uploading them by FTP
-----------------------------------------------------------

This module requires the dsb Client PHP library. But dsb Client itself has many dependencies. Basically, all libraries should be uploaded to your current site's libraries folder. This is located in:

sites/*/libraries

For example:

- sites/all/libraries
- sites/example.com/libraries
- etc

If you don't see a "libraries" folder, simply create one.

After that, all downloaded libraries must be put in their own folder. You will have something like:

sites/*/libraries/{library_name}/

dsb Client:
1. Download it from https://github.com/educach/dsb-client/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "dsb-client" (so you will have sites/*/libraries/dsb-client/)

Sabre.io Xml:
1. Download it from https://github.com/fruux/sabre-xml/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "sabre-xml" (so you will have sites/*/libraries/sabre-xml/)

Sabre.io URI:
1. Download it from https://github.com/fruux/sabre-uri/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "sabre-uri" (so you will have sites/*/libraries/sabre-uri/)

Guzzle:
1. Download *the source code* (NOT the release package) from https://github.com/guzzle/guzzle/releases and unzip it (version 6.2.*).
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "guzzle" (so you will have sites/*/libraries/guzzle/)

Guzzle Psr7:
1. Download it from https://github.com/guzzle/guzzle/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "psr7" (so you will have sites/*/libraries/psr7/)

Guzzle Promises:
1. Download it from https://github.com/guzzle/promises/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and move the folder to "promises" (so you will have sites/*/libraries/promises/)

PHP-FIG HTTP Messages:
1. Download it from https://github.com/php-fig/http-message/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "http-message" (so you will have sites/*/libraries/http-message/)

JeroenDesloovere\VCard:
1. Download it from https://github.com/jeroendesloovere/vcard/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "vcard" (so you will have sites/*/libraries/vcard/)

Behat\Transliterator:
1. Download it from https://github.com/Behat/Transliterator/releases and unzip it.
2. Upload the unzipped folder to your libraries folder (e.g., sites/all/libraries/) and rename the folder to "Transliterator" (so you will have sites/*/libraries/Transliterator/)

Finally, download and enable the following Drupal modules:
- Libraries API (https://www.drupal.org/project/libraries)
- X Autoload (https://www.drupal.org/project/xautoload)

Go to your site status report (admin/reports/status) and make sure Archibald has found all required libraries.


Patching dependencies
=====================

Archibald requires the Revisioning and Field Collection modules to work. However, both modules have an issue that will prevent Archibald from functioning correctly. At the time of writing, both issues have been known and addressed for some time, and a new release containing fixes might come out soon for either module.


Revisioning
-----------

Revisioning has a bug when filtering views based on the revision state. More information can be found here: https://www.drupal.org/node/2070819#comment-10369059 ("Views 'Content revision: State' gives wrong results for Pending state").

Revisioning also has an issue when using Entity Translations. More information can be found here: https://www.drupal.org/node/2750169#comment-11300871 ("Work with Entity Translation").

In order to simplify the installation, Archibald ships with two patch files. In the Archibald module folder, at the root, you will find patch files corresponding to the latest working patches from the issue threads (see above links). Apply them, and the module will start functioning properly.

Note that these patches are required for Revisioning version 7.x-1.9 and below.


Entity Translation
------------------

Entity Translation has a bug when using revisioning. More information can be found here: https://www.drupal.org/node/2750179#comment-11300853 ("Allow default translation handler to be completely overridden").

In order to simplify the installation, Archibald ships with a patch file. In the Archibald module folder, at the root, you will find a patch file corresponding to the latest working patch from the issue thread (see above link). Apply it, and the translations will start functioning properly.

Note that this patch is required for Entity Translation version 7.x-1.0-beta5 and below.


Field Collection
----------------

Field Collection has a bug when using entity translations. More information can be found here: https://www.drupal.org/node/1344672 ("Field Collection: Field translation (entity_translation) support.").

In order to simplify the installation, Archibald ships with a patch file. In the Archibald module folder, at the root, you will find a patch file corresponding to the latest working patch from the issue thread (see above link). Apply it, and the translations will start functioning properly.

Note that this patch is required for Field Collection version 7.x-1.0beta11 and below.


Configuration
=============


First time setup
----------------

When first installed, Archibald will not yet have imported the language and license vocabularies. These can be imported by visiting the admin/reports/status page, and clicking on the appropriate links.

It is a good idea to have a look at the site status report (admin/reports/status), as Archibald provides detailed information on what is missing in the site configuration.

Archibald itself can be configured at admin/config/services/archibald. It ships with sensible defaults, so you can start creating content pretty fast. However, Archibald requires what is known as a "default partner" to fully function.


Connecting to the API ("partners")
----------------------------------

Partners are managed under admin/archibald/partner. They are what make the communication with the national catalog of the Swiss digital school library possible. Each partner has a username, a private key and a passphrase. This data can only be obtained from educa.ch. In order to create partners, your site must have a private file system available. This is necessary to protect the private keys. Archibald will not let you create partners if you haven't setup a private file system. See here how to set up a private file system: https://www.drupal.org/documentation/modules/file#access

Once you have a private file system, you must create at least 1 partner and set it as the "default partner". This will tell Archibald this partner can be used to communicate with the API and fetch Ontology data, which is necessary for creating descriptions.


Managing content
================

All content can be found under admin/archibald.


Descriptions
------------

LOM-CH descriptions are managed at admin/archibald/description. They are Drupal nodes, and can be translated, revisioned, etc.


VCards
------

VCards are managed at admin/archibald/vcard.


Content generation
==================

Archibald implements logic for both devel_generate and devel_contrib_generate. If you wish to generate content to test or play with, install one or both modules, and use their respective UIs to generate content. Note that, for all fields to contain content, you must choose a language for the Description nodes. If the language is neutral, certain fields will not get correctly populated (this is a limitation of both the Field Collection module and the Entity Translation module).
