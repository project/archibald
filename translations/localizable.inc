<?php

/**
 * @file
 * This file has no other purpose as to register translatable fields.
 *
 * We rely on many third-party modules to provide functionality, but these
 * modules don't always have up-to-date translations. We have translators
 * internally, but the process at localize.drupal.org is too slow. We have to
 * be able to ship new versions of our module rapidly, as well as host new
 * instances. We cannot spend time every update or fresh install, to hunt for
 * untranslated strings. So, we register strings that we need to translate, so
 * potx finds them and exports them to our po files.
 */

// entity.module
t('Edit @label');
t('Delete @label');

// revisioning.module
t('Updating existing draft, not creating new revision as this one is still pending.');
t('Initial draft created, pending publication.');
t('Revision operations');
t('%title is @publication_status. It has only one revision');
t('%title is @publication_status. It has @count revisions.');

// views_bulk_operations.module
t('Performed %operation on @items.');
t('Processed @current out of @total');
t('You selected the following <strong>!count</strong>:');
t('item');
t('@count items');

// archibald_curr_mapping
t("This item was automatically added to %curriculum");
t("The following @count items were automatically added to %curriculum");
