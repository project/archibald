<?php

/**
 * @file
 * Contains ArchibaldVcardExporter.
 */

class ArchibaldVcardExporter extends JeroenDesloovere\VCard\VCard {

    /**
     * @see JeroenDesloovere\VCard\VCard::buildVCard()
     */
    public function buildVCardRev($revTimestamp)
    {
        // init string
        $string = "BEGIN:VCARD\r\n";
        $string .= "VERSION:3.0\r\n";
        $string .= "REV:" . date("Y-m-d", $revTimestamp) . "T" . date("H:i:s", $revTimestamp) . "Z\r\n";

        // loop all properties
        $properties = $this->getProperties();
        foreach ($properties as $property) {
            // add to string
            $string .= $this->fold($property['key'] . ':' . $this->escape($property['value']) . "\r\n");
        }

        // add to string
        $string .= "END:VCARD\r\n";

        // return
        return $string;
    }
}
