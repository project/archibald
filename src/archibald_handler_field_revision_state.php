<?php

/**
 * @file
 * Contains archibald_handler_field_revision_state.
 */

class archibald_handler_field_revision_state extends revisioning_handler_field_revision_state {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $nid = $values->{$this->aliases['nid']};
    $vid = $values->{$this->aliases['vid']};
    $published = $values->{$this->aliases['published']};
    $current_vid = $values->{$this->aliases['current_vid']};
    $is_initial_unpublished_draft = !$published && (revisioning_get_number_of_revisions($nid) == 1);
    $is_pending = ($vid > $current_vid) || $is_initial_unpublished_draft;

    // Don't reload the node object. Prepare a new one with the properties we
    // need.
    $node = (object) array(
      'nid' => $nid,
      'vid' => $vid,
      'status' => $published,
      'is_current' => $current_vid == $vid,
      'is_pending' => $is_pending,
    );

    switch (archibald_get_revision_status($node)) {
      case ARCHIBALD_REVISION_STATE_NOT_PUBLISHED:
        return t("No published revision");
      case ARCHIBALD_REVISION_STATE_NEWER_PUBLISHED:
        return t("A newer revision is currently published");
      case ARCHIBALD_REVISION_STATE_OLDER_PUBLISHED:
        return t("An older revision is currently published");
      case ARCHIBALD_REVISION_STATE_CURRENT:
        return t("This is the latest revision");
      case ARCHIBALD_REVISION_STATE_CURRENT_NEW_DRAFT:
        return t("A newer revision is currently in draft");
      default:
        return '-';
    }
  }
}
