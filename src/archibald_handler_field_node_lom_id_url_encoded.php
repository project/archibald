<?php

/**
 * @file
 * Contains archibald_handler_field_node_lom_id_url_encoded.
 */

class archibald_handler_field_node_lom_id_url_encoded extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  function render($values) {
    if (isset($values->{$this->field_alias})) {
      return urlencode($values->{$this->field_alias});
    }
    else {
      return '-';
    }
  }
}
