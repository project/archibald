<?php

/**
 * @file
 * Contains ArchibaldPartnerEntity.
 */

class ArchibaldPartnerEntity extends Entity {

  /**
   * Change the default URI from default/id to archibald/partner/id
   */
  protected function defaultUri() {
    return array('path' => 'archibald/partner/' . $this->identifier());
  }

}
