<?php

/**
 * @file
 * Contains archibald_handler_field_node_lom_id.
 */

class archibald_handler_field_node_lom_id extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  function render($values) {
    if (isset($values->{$this->field_alias})) {
      return $values->{$this->field_alias};
    }
    else {
      return '-';
    }
  }
}
