<?php

/**
 * @file
 * Contains ArchibaldApiTestClient.
 */

use Educa\DSB\Client\ApiClient\TestClient;

class ArchibaldApiTestClient extends TestClient {

  public function deleteDescription($id) {
    drupal_set_message("Received a delete request for $id");
    return parent::deleteDescription($id);
  }
}
