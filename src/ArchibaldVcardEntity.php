<?php

/**
 * @file
 * Contains ArchibaldVcardEntity.
 */

class ArchibaldVcardEntity extends Entity {

  /**
   * Change the default URI from default/id to archibald/vcard/id
   */
  protected function defaultUri() {
    return array('path' => 'archibald/vcard/' . $this->identifier());
  }

}
