/**
 * @file
 * JS enhancements for the archibald_lomch_description node form.
 *
 * The first enhancement is the "footer" fieldset navigation. Inside each node
 * form fieldset that is part of the vertical tab group, we add links in the
 * footer, to navigate to the previous or next fieldset.
 *
 * The second is to allow the collapsing of the vertical tabs.
 */
(function($, Drupal) {

  Drupal.behaviors.archibaldLomchDescriptionNodeForm = {

    attach: function(context, settings) {
      if (settings.archibald.activateVTabFooterLinks) {
        // Add footer links to the vertical tabs.
        var $vTabPanes = $('.vertical-tabs-panes > fieldset:not(.js-archibald-processed)', context);
        if ($vTabPanes.length) {
          $vTabPanes.each(function() {
            var $fieldset = $(this),
                verticalTab = $fieldset.data('verticalTab');

            if (verticalTab) {
              $fieldset.append(Drupal.theme('archibald_vtab_footer', {
                prev: $fieldset.prev('fieldset'),
                next: $fieldset.next('fieldset')
              }));
            }
          });
          $vTabPanes.addClass('js-archibald-processed');
        }
      }

      if (settings.archibald.activateCollapsibleVTabs) {
        // Allow the collapsing of the vertical tabs.
        var $vTabWrapper = $('.vertical-tabs:not(.js-archibald-processed)', context);
        if ($vTabWrapper.length) {
          $vTabWrapper.each(function() {
            var $this = $(this),
                $collapseLink = $('<a class="archibald-vertical-tabs-collapse-link clearfix">' + Drupal.theme('archibald_vtab_collapse_link') + '</a>');

            $collapseLink.click(function() {
              $this.toggleClass('archibald-vertical-tabs-collapsed');
              if ($this.hasClass('archibald-vertical-tabs-collapsed')) {
                $collapseLink.text(Drupal.theme('archibald_vtab_collapse_link', { collapsed: true }));
              }
              else {
                $collapseLink.text(Drupal.theme('archibald_vtab_collapse_link'));
              }
            });

            $this.before($collapseLink);
          });
          $vTabWrapper.addClass('js-archibald-processed');
        }
      }

      if (settings.archibald.activateStickyVTabs) {
        // Make the vertical tabs sticky.
        var $vTabs = $('.vertical-tabs-list:not(.js-archibald-processed)', context);
        if ($vTabs.length) {
          $vTabs.sticky({ topSpacing: 30 });
          // The width is a bit off depending on the theme. Make sure the new
          // sticky wrapper has the same width as the vertical tabs.
          $vTabs.each(function() {
            var $this = $(this);
            $this.parent().width($this.width());
          });
          $vTabs.addClass('js-archibald-processed');
        }
      }
    }

  };

  Drupal.theme.archibald_vtab_footer = function(vars) {
    var $footer = $('<div class="archibald-vertical-tabs-pane-footer clearfix"></div>');

    if (vars.prev !== undefined && vars.prev.length) {
      var $prev = $('<a class="archibald-vertical-tabs-pane-footer__link archibald-vertical-tabs-pane-footer__link--prev-link">‹ ' + vars.prev.find('> legend').text() + '</a>');
      $prev.data('fieldset', vars.prev);

      $prev.click(function() {
        $prev.data('fieldset').data('verticalTab').focus();
      });

      $footer.append($prev);
    }

    if (vars.next !== undefined && vars.next.length) {
      var $next = $('<a class="archibald-vertical-tabs-pane-footer__link archibald-vertical-tabs-pane-footer__link--next-link">' + vars.next.find('> legend').text() + ' ›</a>');
      $next.data('fieldset', vars.next);

      $next.click(function() {
        $next.data('fieldset').data('verticalTab').focus();
      });

      $footer.append($next);
    }

    return $footer;
  };

  Drupal.theme.archibald_vtab_collapse_link = function(vars) {
    if (vars && vars.collapsed) {
      return Drupal.t("Expand vertical tabs") + ' ›';
    }
    else {
      return '‹ ' + Drupal.t("Collapse vertical tabs");
    }
  };

})(jQuery, Drupal);
